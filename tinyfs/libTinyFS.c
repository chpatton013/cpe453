#include "libTinyFS.h"
#include "libDisk.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

///////////////////////////////////////////////////////////////////////////////
// Definitions.
///////////////////////////////////////////////////////////////////////////////

#define MIN_NUM_BLOCKS 4
#define MAX_NUM_BLOCKS UINT8_MAX
#define FILE_TABLE_LENGTH 16

#define SUPER_BLOCK_ADDR 0
#define ROOT_BLOCK_ADDR 1
#define MAGIC 0x45
#define MAX_FILE_NAME_LENGTH 9

///////////////////////////////////////////////////////////////////////////////
// Types.
///////////////////////////////////////////////////////////////////////////////

#define PACKED_STRUCT struct __attribute__((__packed__))
enum block_type_t { SUPER = 1, DIRECTORY, INODE, DATA, FREE };

typedef PACKED_STRUCT block_head {
   unsigned char type;
   unsigned char magic;
   unsigned char addr;
   unsigned char empty;
} block_head_t;

#define BLOCK_DATA_SIZE (BLOCKSIZE - sizeof(block_head_t))
typedef PACKED_STRUCT block {
   block_head_t head;
   unsigned char data[BLOCK_DATA_SIZE];
} block_t;

typedef PACKED_STRUCT super_data {
   uint32_t nBlocks;
   uint32_t nFreeBlocks;
   unsigned char rootAddr;
   unsigned char freeAddr;
} super_data_t;

typedef PACKED_STRUCT meta_data {
   unsigned char RO;
   uint64_t cTime;
   uint64_t mTime;
   uint64_t aTime;
   char name[MAX_FILE_NAME_LENGTH];
} meta_data_t;

typedef PACKED_STRUCT dir_meta_data {
   meta_data_t meta;
   unsigned char nFiles;
} dir_meta_data_t;

#define DIR_FILE_ARRAY_LENGTH (BLOCK_DATA_SIZE - sizeof(dir_meta_data_t))
typedef PACKED_STRUCT dir_data {
   dir_meta_data_t dirMeta;
   unsigned char fileAddrs[DIR_FILE_ARRAY_LENGTH];
} dir_data_t;

typedef PACKED_STRUCT file_data {
   meta_data_t meta;
   uint32_t size;
} file_data_t;

typedef PACKED_STRUCT fd_row {
   unsigned char addr;
   unsigned char dirAddr;
   unsigned int cursor;
} fd_row_t;

///////////////////////////////////////////////////////////////////////////////
// Prototypes.
///////////////////////////////////////////////////////////////////////////////

static int bytesToBlocks(int nBytes);

static int tfs_fsck(int disk);

static void initBlock(block_t* block, char type, unsigned char addr);
static void initDirBlock(block_t* block, char* name);
static void initFileBlock(block_t* block, unsigned char dataAddr, char* name);
static void initDataBlock(block_t* block);

static int findFile(char* name);
static int findFileInDir(char* name, unsigned char dirAddr);
static int findFileContainer(char* name);
static int findFileContainerHelper(char** pathPieces, unsigned char dirAddr);

static char** getPathPieces(char* name);
static char* getFinalPathPiece(char* name);

static int makeFile(char* name);
static int deleteFile(unsigned char fileAddr, unsigned char dirAddr);
static int deleteDir(unsigned char dirAddr, unsigned char parentDirAddr);

static int addFileToDir(unsigned char fileAddr, unsigned char dirAddr);
static int removeFileFromDir(unsigned char fileAddr, unsigned char dirAddr);
static int removeAllHelper(unsigned char dirAddr, unsigned char parentDirAddr);

static int deallocateFile(unsigned char addr);
static int addBlockToFreeList(unsigned char addr);

static fileDescriptor addFileToTable(unsigned char addr, unsigned char dirAddr);

static char isActiveFD(fileDescriptor FD);
static char isValidName(char* name);

static int getFDBlock(fileDescriptor FD, block_t* block);
static int getFileInode(fileDescriptor FD, file_data_t* fileData);

static int listDiskHelper(unsigned char dirAddr, int indent);
static int listDirContents(dir_data_t* dirData, int indent);

static int writeFileData(char* buffer, int nBytes);
static int requisitionBlockList(int nBlocks);
static int requisitionBlock();

static int getDataBlockAtOffset(unsigned char addr, unsigned int offset);

static void printFileTable();
static void printBlock(unsigned char addr);
static void printSuperBlock(super_data_t* superData);
static void printDirectoryBlock(dir_data_t* dirData);
static void printInodeBlock(file_data_t* fileData);
static void printMetaData(meta_data_t* metaData);

///////////////////////////////////////////////////////////////////////////////
// Data.
///////////////////////////////////////////////////////////////////////////////

static int tfsDisk = -1;
static fd_row_t* fileTable = NULL;
static int fileTableSize = 0;
static int fileTableLength = 0;

///////////////////////////////////////////////////////////////////////////////
// Public API.
///////////////////////////////////////////////////////////////////////////////

int tfs_mkfs(char* filename, int nBytes) {
   int nBlocks = nBytes / BLOCKSIZE;
   if (nBlocks > 0 && (nBlocks < MIN_NUM_BLOCKS || nBytes % BLOCKSIZE)) {
      return TFS_ESIZE;
   }

   int disk = openDisk(filename, nBytes);
   if (disk < -1) {
      return TFS_ESIZE;
   } else if (disk < 0) {
      return TFS_EACCESS;
   }

   int ndx, freeAddr;
   block_t block;
   super_data_t* superData;

   freeAddr = ROOT_BLOCK_ADDR + 1;

   // Super block.
   initBlock(&block, SUPER, 0);
   superData = (super_data_t*)(&block.data);
   superData->nBlocks = nBlocks;
   superData->nFreeBlocks = nBlocks - 2;
   superData->rootAddr = ROOT_BLOCK_ADDR;
   superData->freeAddr = freeAddr;
   if (writeBlock(disk, SUPER_BLOCK_ADDR, &block) == -1) {
      return TFS_EACCESS;
   }

   // Root directory.
   initDirBlock(&block, "");
   if (writeBlock(disk, ROOT_BLOCK_ADDR, &block) == -1) {
      return TFS_EACCESS;
   }

   // Free List.
   for (ndx = freeAddr; ndx < nBlocks; ++ndx) {
      initBlock(&block, FREE, (ndx + 1) % nBlocks);
      if (writeBlock(disk, ndx, &block) == -1) {
         return TFS_EACCESS;
      }
   }

   if (closeDisk(disk) == -1) {
      return TFS_EACCESS;
   }

   return TFS_SUCCESS;
}

int tfs_mount(char* filename) {
   if (tfsDisk != -1) {
      return TFS_EONEFS;
   }

   int disk = openDisk(filename, 0);
   if (disk == -1) {
      return TFS_EACCESS;
   }

   int err = tfs_fsck(disk);
   if (err != TFS_SUCCESS) {
      return err;
   }

   tfsDisk = disk;

   fileTable = (fd_row_t*)malloc(FILE_TABLE_LENGTH * sizeof(fd_row_t));
   fileTableSize = 0;
   fileTableLength = FILE_TABLE_LENGTH;

   return TFS_SUCCESS;
}

int tfs_unmount() {
   if (tfsDisk == -1) {
      return TFS_ENOFS;
   }

   free(fileTable);
   fileTableSize = fileTableLength = 0;
   fileTable = NULL;

   if (closeDisk(tfsDisk) == -1) {
      return TFS_EACCESS;
   }
   tfsDisk = -1;

   return TFS_SUCCESS;
}

int tfs_listDisk() {
   block_t block;
   if (readBlock(tfsDisk, SUPER_BLOCK_ADDR, &block) == -1) {
      return TFS_EACCESS;
   }
   super_data_t* superData = (super_data_t*)(&block.data);
   return listDiskHelper(superData->rootAddr, 0);
}

int tfs_readdir() { return tfs_listDisk(); }

int tfs_createDir(char* dirName) {
   // Make sure a file with this name doesn't already exist.
   int fileAddr = findFile(dirName);
   if (fileAddr > 0) {
      return TFS_EEXISTS;
   } else if (fileAddr != TFS_ENOTFOUND) {
      return fileAddr;
   }

   // Get the directory container's address.
   int parentDirAddr = findFileContainer(dirName);
   if (parentDirAddr < 0) {
      return parentDirAddr;
   }
   // Get the directory's container.
   block_t parentDirBlock;
   if (readBlock(tfsDisk, parentDirAddr, &parentDirBlock) == -1) {
      return TFS_EACCESS;
   }
   dir_data_t* parentDirData = (dir_data_t*)(&parentDirBlock.data);

   // Do not allow modification of read-only file.
   if (parentDirData->dirMeta.meta.RO) {
      return TFS_ERO;
   }

   char* fileName = getFinalPathPiece(dirName);
   if (!isValidName(fileName)) {
      free(fileName);
      return TFS_ENAME;
   }

   // Requisition block for new directory.
   int dirAddr = requisitionBlock();
   if (dirAddr < 0) {
      return fileAddr;
   }
   // Initialize the new directory block.
   block_t dirBlock;
   initDirBlock(&dirBlock, fileName);
   free(fileName);
   // Write the new directory block.
   if (writeBlock(tfsDisk, dirAddr, &dirBlock) == -1) {
      return TFS_EACCESS;
   }

   // And add the directory to its parent.
   int err = addFileToDir(dirAddr, parentDirAddr);
   if (err < 0) {
      return err;
   }

   return TFS_SUCCESS;
}

int tfs_removeDir(char* dirName) {
   // Get the address of this directory.
   int dirAddr = findFile(dirName);
   if (dirAddr < 0) {
      return dirAddr;
   }
   // Get the address of the directory's container.
   int parentDirAddr = findFileContainer(dirName);
   if (parentDirAddr < 0) {
      return parentDirAddr;
   }

   return deleteDir(dirAddr, parentDirAddr);
}

int tfs_listDir(char* dirName) {
   // Get the address of this directory.
   int dirAddr = findFile(dirName);
   if (dirAddr < 0) {
      return dirAddr;
   }
   // Get the directory block.
   block_t block;
   if (readBlock(tfsDisk, dirAddr, &block) == -1) {
      return TFS_EACCESS;
   }
   // Make sure this block is a directory.
   if (block.head.type != DIRECTORY) {
      return TFS_ETYPE;
   }
   // List the files in the directory.
   dir_data_t* dirData = (dir_data_t*)(&block.data);
   listDirContents(dirData, 0);

   return TFS_SUCCESS;
}

int tfs_removeAll(char* dirName) {
   if (dirName[0] == '/') {
      ++dirName;
   }

   block_t dirBlock;

   // Get the address of this directory.
   int dirAddr;
   if (!strcmp(dirName, "")) {
      if (readBlock(tfsDisk, SUPER_BLOCK_ADDR, &dirBlock) == -1) {
         return TFS_EACCESS;
      }
      super_data_t* superData = (super_data_t*)(&dirBlock.data);
      dirAddr = superData->rootAddr;
      if (readBlock(tfsDisk, dirAddr, &dirBlock) == -1) {
         return TFS_EACCESS;
      }
   } else {
      dirAddr = findFile(dirName);
      if (dirAddr < 0) {
         return dirAddr;
      }
      if (readBlock(tfsDisk, dirAddr, &dirBlock) == -1) {
         return TFS_EACCESS;
      }
   }

   // Make sure this block is a directory.
   if (dirBlock.head.type != DIRECTORY) {
      return TFS_ETYPE;
   }

   dir_data_t* dirData = (dir_data_t*)(&dirBlock.data);
   int ndx, nFiles = dirData->dirMeta.nFiles;
   for (ndx = nFiles - 1; ndx >= 0; --ndx) {
      int err = removeAllHelper(dirData->fileAddrs[ndx], dirAddr);
      if (err < 0) {
         return err;
      }
   }

   if (!strcmp(dirName, "")) {
      return TFS_SUCCESS;
   } else {
      return tfs_removeDir(dirName);
   }
}

fileDescriptor tfs_openFile(char* name) {
   int dirAddr = findFileContainer(name);
   if (dirAddr < 0) {
      return dirAddr;
   }

   int fileAddr = findFile(name);
   if (fileAddr == TFS_ENOTFOUND) {
      char* fileName = getFinalPathPiece(name);
      fileAddr = makeFile(fileName);
      free(fileName);

      if (fileAddr < 0) {
         return fileAddr;
      }

      addFileToDir(fileAddr, dirAddr);
   } else if (fileAddr < 0) {
      return fileAddr;
   }

   return addFileToTable(fileAddr, dirAddr);
}

int tfs_closeFile(fileDescriptor FD) {
   if (!isActiveFD(FD)) {
      return TFS_EBADFD;
   }

   fileTable[FD].addr = 0;

   return TFS_SUCCESS;
}

int tfs_writeFile(fileDescriptor FD, char* buffer, int size) {
   if (!isActiveFD(FD)) {
      return TFS_EBADFD;
   }

   if (size < 0) {
      return TFS_ESIZE;
   }

   block_t block;
   int dataAddr;

   // Find the number of free blocks.
   if (readBlock(tfsDisk, SUPER_BLOCK_ADDR, &block) == -1) {
      return TFS_EACCESS;
   }
   super_data_t* superData = (super_data_t*)(&block.data);
   int nFreeBlocks = superData->nFreeBlocks;

   // Fetch inode block.
   unsigned char fileAddr = fileTable[FD].addr;
   if (readBlock(tfsDisk, fileAddr, &block) == -1) {
      return TFS_EACCESS;
   }
   file_data_t* fileData = (file_data_t*)(&block.data);

   // Do not allow modification of read-only file.
   if (fileData->meta.RO) {
      return TFS_ERO;
   }

   // Find the number of data blocks currently allocated to this file.
   int nDataBlocks = bytesToBlocks(fileData->size);

   // Find the number of blocks needed to write this new buffer.
   int nBlocks = bytesToBlocks(size);
   if (nFreeBlocks + nDataBlocks < nBlocks) {
      return TFS_ESPACE;
   }

   // Find start of current data blocks.
   dataAddr = block.head.addr;
   // Check for unusual data inconsistency.
   if (dataAddr <= 0) {
      return TFS_EUNKNOWN;
   }

   // De-allocate data blocks.
   int err = deallocateFile(dataAddr);
   if (err < 0) {
      return err;
   }

   // Re-allocate data blocks.
   dataAddr = writeFileData(buffer, size);
   if (dataAddr < 0) {
      return dataAddr;
   }

   // Setup data pointer.
   block.head.addr = dataAddr;
   // And inode properties.
   fileData = (file_data_t*)(&block.data);
   fileData->size = size;
   fileData->meta.mTime = time(NULL);
   // Then flush the inode block.
   if (writeBlock(tfsDisk, fileAddr, &block) == -1) {
      return TFS_EACCESS;
   }

   fileTable[FD].cursor = 0;

   return TFS_SUCCESS;
}

int tfs_deleteFile(fileDescriptor FD) {
   if (!isActiveFD(FD)) {
      return TFS_EBADFD;
   }

   int err = deleteFile(fileTable[FD].addr, fileTable[FD].dirAddr);
   if (err < 0) {
      return err;
   }

   return tfs_closeFile(FD);
}

int tfs_readByte(fileDescriptor FD, char* buffer) {
   if (!isActiveFD(FD)) {
      return TFS_EBADFD;
   }

   block_t fileBlock;

   if (readBlock(tfsDisk, fileTable[FD].addr, &fileBlock) == -1) {
      return TFS_EACCESS;
   }
   file_data_t* fileData = (file_data_t*)(&fileBlock.data);

   // Already at EOF.
   if (fileData->size <= fileTable[FD].cursor) {
      return TFS_EDATA;
   }

   // Find the appropriate data block.
   int dataAddr = getDataBlockAtOffset(fileBlock.head.addr, fileTable[FD].cursor);
   if (dataAddr < 0) {
      return dataAddr;
   }

   block_t dataBlock;

   // Read appropriate data block.
   if (readBlock(tfsDisk, dataAddr, &dataBlock) == -1) {
      return TFS_EACCESS;
   }

   // Update inode properties.
   fileData->meta.aTime = time(NULL);
   if (writeBlock(tfsDisk, fileTable[FD].addr, &fileBlock) == -1) {
      return TFS_EACCESS;
   }

   // Read data from block into buffer.
   *buffer = dataBlock.data[fileTable[FD].cursor % BLOCK_DATA_SIZE];
   // And advance the cursor.
   ++fileTable[FD].cursor;

   return TFS_SUCCESS;
}

int tfs_writeByte(fileDescriptor FD, unsigned char data) {
   if (!isActiveFD(FD)) {
      return TFS_EBADFD;
   }

   // Fetch inode.
   block_t fileBlock;
   unsigned char fileAddr = fileTable[FD].addr;
   if (readBlock(tfsDisk, fileAddr, &fileBlock) == -1) {
      return TFS_EACCESS;
   }
   file_data_t* fileData = (file_data_t*)(&fileBlock.data);

   // Do not allow modification of read-only file.
   if (fileData->meta.RO) {
      return TFS_ERO;
   }

   // Get the data block under the cursor.
   block_t dataBlock;
   int dataAddr = getDataBlockAtOffset(fileBlock.head.addr, fileTable[FD].cursor);
   if (readBlock(tfsDisk, dataAddr, &dataBlock) == -1) {
      return TFS_EACCESS;
   }

   char eof = fileTable[FD].cursor == fileData->size;

   // Requisition and link new data block if necessary.
   int nBlocksAllocated = bytesToBlocks(fileData->size);
   int nBlocksNeeded = bytesToBlocks(fileTable[FD].cursor);
   if (eof && nBlocksNeeded > nBlocksAllocated) {
      int nextDataAddr = requisitionBlock();
      if (nextDataAddr < 0) {
         return nextDataAddr;
      }

      dataBlock.head.addr = nextDataAddr;
      if (writeBlock(tfsDisk, dataAddr, &dataBlock) == -1) {
         return TFS_EACCESS;
      }
      dataAddr = nextDataAddr;
   }

   // Write the data.
   dataBlock.data[fileTable[FD].cursor % BLOCK_DATA_SIZE] = data;
   if (writeBlock(tfsDisk, dataAddr, &dataBlock) == -1) {
      return TFS_EACCESS;
   }
   // Advance the cursor.
   ++fileTable[FD].cursor;

   if (eof) {
      ++fileData->size;
   }

   // Update the inode.
   fileData->meta.mTime = time(NULL);
   if (writeBlock(tfsDisk, fileAddr, &fileBlock) == -1) {
      return TFS_EACCESS;
   }

   return TFS_SUCCESS;
}

int tfs_seek(fileDescriptor FD, int offset) {
   if (!isActiveFD(FD)) {
      return TFS_EBADFD;
   }

   // Get inode.
   block_t fileBlock;
   unsigned char fileAddr = fileTable[FD].addr;
   if (readBlock(tfsDisk, fileAddr, &fileBlock) == -1) {
      return TFS_EACCESS;
   }
   file_data_t* fileData = (file_data_t*)(&fileBlock.data);

   // Do not allow modification of read-only file.
   if (fileData->meta.RO && offset > fileData->size) {
      return TFS_ERO;
   }

   int nBlocksAllocated = bytesToBlocks(fileData->size);
   int nBlocksNeeded = bytesToBlocks(offset);

   // Bail early if this seek will not allocate new blocks.
   if (nBlocksNeeded <= nBlocksAllocated) {
      fileTable[FD].cursor = offset;
      return TFS_SUCCESS;
   }

   // Get necessary data blocks.
   int nextDataAddr = requisitionBlockList(nBlocksNeeded);
   if (nextDataAddr < 0) {
      return nextDataAddr;
   }

   // Get current last data block for this file.
   block_t dataBlock;
   int dataAddr = getDataBlockAtOffset(fileBlock.head.addr, fileData->size);
   if (dataAddr < 0) {
      return dataAddr;
   }
   if (readBlock(tfsDisk, dataAddr, &dataBlock) == -1) {
      return TFS_EACCESS;
   }

   // Link it to the head of the newly requisistioned list.
   dataBlock.head.addr = nextDataAddr;
   if (writeBlock(tfsDisk, dataAddr, &dataBlock) == -1) {
      return TFS_EACCESS;
   }

   // Update the inode.
   fileData->meta.mTime = time(NULL);
   fileData->size = offset;
   if (writeBlock(tfsDisk, fileAddr, &fileBlock) == -1) {
      return TFS_EACCESS;
   }

   return TFS_SUCCESS;
}

int tfs_rename(fileDescriptor FD, char* newName) {
   if (!isValidName(newName)) {
      return TFS_ENAME;
   }

   int err = findFileInDir(newName, fileTable[FD].dirAddr);
   if (err >= 0) {
      return TFS_EEXISTS;
   } else if (err != TFS_ENOTFOUND) {
      return err;
   }

   // Fetch inode block.
   block_t fileBlock;
   unsigned char fileAddr = fileTable[FD].addr;
   if (readBlock(tfsDisk, fileAddr, &fileBlock) == -1) {
      return TFS_EACCESS;
   }
   file_data_t* fileData = (file_data_t*)(&fileBlock.data);

   // Do not allow modification of read-only file.
   if (fileData->meta.RO) {
      return TFS_ERO;
   }

   fileData->meta.mTime = time(NULL);
   strncpy(fileData->meta.name, newName, MAX_FILE_NAME_LENGTH);
   if (writeBlock(tfsDisk, fileAddr, &fileBlock) == -1) {
      return TFS_EACCESS;
   }

   return TFS_SUCCESS;
}

int tfs_makeRO(char* name) {
   int fileAddr = findFile(name);
   if (fileAddr < 0) {
      return fileAddr;
   }

   block_t block;
   if (readBlock(tfsDisk, fileAddr, &block) == -1) {
      return TFS_EACCESS;
   }
   file_data_t* fileData = (file_data_t*)(&block.data);

   if (fileData->meta.RO) {
      return TFS_ERO;
   }

   fileData->meta.RO = 1;
   fileData->meta.mTime = time(NULL);
   if (writeBlock(tfsDisk, fileAddr, &block) == -1) {
      return TFS_EACCESS;
   }

   return TFS_SUCCESS;
}

int tfs_makeRW(char* name) {
   int fileAddr = findFile(name);
   if (fileAddr < 0) {
      return fileAddr;
   }

   block_t block;
   if (readBlock(tfsDisk, fileAddr, &block) == -1) {
      return TFS_EACCESS;
   }
   file_data_t* fileData = (file_data_t*)(&block.data);

   if (!fileData->meta.RO) {
      return TFS_ERW;
   }

   fileData->meta.RO = 0;
   fileData->meta.mTime = time(NULL);
   if (writeBlock(tfsDisk, fileAddr, &block) == -1) {
      return TFS_EACCESS;
   }

   return TFS_SUCCESS;
}

int tfs_getRO(fileDescriptor FD) {
   file_data_t fileData;
   int err = getFileInode(FD, &fileData);
   if (err != TFS_SUCCESS) {
      return err;
   }

   return fileData.meta.RO;
}

int tfs_getRW(fileDescriptor FD) {
   file_data_t fileData;
   int err = getFileInode(FD, &fileData);
   if (err != TFS_SUCCESS) {
      return err;
   }

   return !fileData.meta.RO;
}

uint64_t tfs_getSize(fileDescriptor FD) {
   file_data_t fileData;
   int err = getFileInode(FD, &fileData);
   if (err != TFS_SUCCESS) {
      return err;
   }

   return fileData.size;
}

time_t tfs_getCTime(fileDescriptor FD) {
   file_data_t fileData;
   int err = getFileInode(FD, &fileData);
   if (err != TFS_SUCCESS) {
      return err;
   }

   return fileData.meta.cTime;
}

time_t tfs_getMTime(fileDescriptor FD) {
   file_data_t fileData;
   int err = getFileInode(FD, &fileData);
   if (err != TFS_SUCCESS) {
      return err;
   }

   return fileData.meta.mTime;
}

time_t tfs_getATime(fileDescriptor FD) {
   file_data_t fileData;
   int err = getFileInode(FD, &fileData);
   if (err != TFS_SUCCESS) {
      return err;
   }

   return fileData.meta.aTime;
}

time_t tfs_readFileInfo(fileDescriptor FD) { return tfs_getCTime(FD); }

///////////////////////////////////////////////////////////////////////////////
// Private API.
///////////////////////////////////////////////////////////////////////////////

int bytesToBlocks(int nBytes) {
   int nBlocks = nBytes / BLOCK_DATA_SIZE;
   if (nBytes % BLOCK_DATA_SIZE) {
      ++nBlocks;
   }

   return nBlocks;
}

int tfs_fsck(int disk) {
   block_t block;

   if (readBlock(disk, SUPER_BLOCK_ADDR, &block) == -1) {
      return TFS_EACCESS;
   }

   if (block.head.magic != MAGIC) {
      return TFS_ENOTFS;
   }

   int ndx;
   super_data_t* superData = (super_data_t*)(&block.data);
   for (ndx = ROOT_BLOCK_ADDR; ndx < superData->nBlocks; ++ndx) {
      if (readBlock(disk, ndx, &block) == -1) {
         return TFS_EACCESS;
      }

      if (block.head.magic != MAGIC) {
         return TFS_ENOTFS;
      }
   }

   return TFS_SUCCESS;
}

void initBlock(block_t* block, char type, unsigned char addr) {
   block->head.type = type;
   block->head.magic = MAGIC;
   block->head.addr = addr;
   block->head.empty = 0;

   memset(block->data, 0, BLOCK_DATA_SIZE);
}

void initDirBlock(block_t* block, char* name) {
   initBlock(block, DIRECTORY, 0);

   uint64_t now = time(NULL);
   dir_data_t* dirData = (dir_data_t*)(block->data);

   dirData->dirMeta.nFiles = 0;

   dirData->dirMeta.meta.RO = 0;
   dirData->dirMeta.meta.cTime = now;
   dirData->dirMeta.meta.mTime = now;
   dirData->dirMeta.meta.aTime = now;

   strncpy(dirData->dirMeta.meta.name, name, MAX_FILE_NAME_LENGTH);
}

void initFileBlock(block_t* block, unsigned char dataAddr, char* name) {
   initBlock(block, INODE, dataAddr);

   uint64_t now = time(NULL);
   file_data_t* fileData = (file_data_t*)(block->data);

   fileData->size = 0;

   fileData->meta.RO = 0;
   fileData->meta.cTime = now;
   fileData->meta.mTime = now;
   fileData->meta.aTime = now;

   strncpy(fileData->meta.name, name, MAX_FILE_NAME_LENGTH);
}

void initDataBlock(block_t* block) {
   initBlock(block, DATA, 0);
}

int findFile(char* name) {
   int dirAddr = findFileContainer(name);
   if (dirAddr < 0) {
      return dirAddr;
   }

   char* fileName = getFinalPathPiece(name);
   int fileAddr = findFileInDir(fileName, dirAddr);
   free(fileName);

   return fileAddr;
}

int findFileInDir(char* name, unsigned char dirAddr) {
   // Get directory block.
   block_t dirBlock;
   if (readBlock(tfsDisk, dirAddr, &dirBlock) == -1) {
      return TFS_EACCESS;
   }
   dir_data_t* dirData = (dir_data_t*)(&dirBlock.data);

   // Seach this directory for files with the specified name.
   int ndx, nFiles;
   nFiles = dirData->dirMeta.nFiles;
   for (ndx = 0; ndx < nFiles; ++ndx) {
      // Get referenced file block.
      block_t fileBlock;
      unsigned char fileAddr = dirData->fileAddrs[ndx];
      if (readBlock(tfsDisk, fileAddr, &fileBlock) == -1) {
         return TFS_EACCESS;
      }
      // Check for name match.
      file_data_t* fileData = (file_data_t*)(&fileBlock.data);
      if (!strcmp(name, fileData->meta.name)) {
         return fileAddr;
      }
   }

   return TFS_ENOTFOUND;
}

int findFileContainer(char* name) {
   // Get root inode address from super block.
   block_t block;
   if (readBlock(tfsDisk, SUPER_BLOCK_ADDR, &block) == -1) {
      return TFS_EACCESS;
   }
   super_data_t* superData = (super_data_t*)(&block.data);
   unsigned char dirAddr = superData->rootAddr;

   // Decompose path into pieces.
   char** pathPieces = getPathPieces(name);
   // Advance to the end of the list.
   int ndx;
   for (ndx = 0; pathPieces[ndx] != NULL; ++ndx) {
   }

   // This shouldn't happen, but just in case.
   if (ndx == 0) {
      free(pathPieces);
      return dirAddr;
   }

   // Remove final path component from the list.
   free(pathPieces[ndx - 1]);
   pathPieces[ndx - 1] = NULL;
   // Find address of last path piece still in list.
   int fileAddr = findFileContainerHelper(pathPieces, dirAddr);
   // Cleanup path pieces.
   for (ndx = 0; pathPieces[ndx] != NULL; ++ndx) {
      free(pathPieces[ndx]);
   }
   free(pathPieces);

   return fileAddr;
}

int findFileContainerHelper(char** pathPieces, unsigned char dirAddr) {
   char* fileName = pathPieces[0];
   // Recursion base-case.
   if (fileName == NULL) {
      return dirAddr;
   }

   // Get directory block.
   block_t dirBlock;
   if (readBlock(tfsDisk, dirAddr, &dirBlock) == -1) {
      return TFS_EACCESS;
   }
   // If there are more path pieces verify that this block is a directory.
   if (pathPieces[1] != NULL && dirBlock.head.type != DIRECTORY) {
      return TFS_ENOTFOUND;
   }

   dir_data_t* dirData = (dir_data_t*)(&dirBlock.data);

   int fileAddr = 0;
   // Seach this directory for files with the specified name.
   int ndx;
   for (ndx = 0; ndx < dirData->dirMeta.nFiles; ++ndx) {
      // Get referenced file block.
      block_t fileBlock;
      if (readBlock(tfsDisk, dirData->fileAddrs[ndx], &fileBlock) == -1) {
         return TFS_EACCESS;
      }
      // Check for name match.
      file_data_t* fileData = (file_data_t*)(&fileBlock.data);
      if (!strcmp(fileName, fileData->meta.name)) {
         fileAddr = dirData->fileAddrs[ndx];
         break;
      }
   }

   // File not found.
   if (fileAddr == 0) {
      return TFS_ENOTFOUND;
   }

   return findFileContainerHelper(pathPieces + 1, fileAddr);
}

char** getPathPieces(char* name) {
   if (name[0] == '/') {
      ++name;
   }

   int ndx, level = 1;
   for (ndx = 0; name[ndx] != '\0'; ++ndx) {
      if (name[ndx] == '/') {
         ++level;
      }
   }

   char* path = strdup(name);
   char** pathPieces = (char**)malloc((level + 1) * sizeof(char*));

   char* tok = strtok(path, "/");

   ndx = 0;
   do {
      pathPieces[ndx] = strdup(tok);
      tok = strtok(NULL, "/");
      ++ndx;
   } while (tok != NULL);
   pathPieces[ndx] = NULL;

   free(path);

   return pathPieces;
}

char* getFinalPathPiece(char* name) {
   char** pathPieces = getPathPieces(name);
   int ndx;
   for (ndx = 0; pathPieces[ndx] != NULL; ++ndx) {
   }

   if (ndx == 0) {
      free(pathPieces);
      return NULL;
   }

   char* fileName = strdup(pathPieces[ndx - 1]);
   free(pathPieces);
   return fileName;
}

int makeFile(char* name) {
   if (!isValidName(name)) {
      return TFS_ENAME;
   }

   // Fetch the needed blocks for file creation.
   block_t superBlock;
   if (readBlock(tfsDisk, SUPER_BLOCK_ADDR, &superBlock) == -1) {
      return TFS_EACCESS;
   }
   super_data_t* superData = (super_data_t*)(&superBlock.data);
   if (superData->nFreeBlocks < 2) {
      return TFS_ESPACE;
   }

   unsigned char inodeAddr = superData->freeAddr;
   if (inodeAddr == 0) {
      return TFS_ESPACE;
   }

   block_t inodeBlock;
   if (readBlock(tfsDisk, inodeAddr, &inodeBlock) == -1) {
      return TFS_EACCESS;
   }

   unsigned char dataAddr = inodeBlock.head.addr;
   if (dataAddr == 0) {
      return TFS_ESPACE;
   }

   block_t dataBlock;
   if (readBlock(tfsDisk, dataAddr, &dataBlock) == -1) {
      return TFS_EACCESS;
   }

   // Update the free list pointer.
   superData->freeAddr = dataBlock.head.addr;
   if (writeBlock(tfsDisk, SUPER_BLOCK_ADDR, &superBlock) == -1) {
      return TFS_EACCESS;
   }

   // Create the inode.
   initFileBlock(&inodeBlock, dataAddr, name);
   if (writeBlock(tfsDisk, inodeAddr, &inodeBlock) == -1) {
      return TFS_EACCESS;
   }

   // Create the file-extent.
   initDataBlock(&dataBlock);
   if (writeBlock(tfsDisk, dataAddr, &dataBlock) == -1) {
      return TFS_EACCESS;
   }

   return inodeAddr;
}

int deleteFile(unsigned char fileAddr, unsigned char dirAddr) {
   // Fetch inode block.
   block_t block;
   if (readBlock(tfsDisk, fileAddr, &block) == -1) {
      return TFS_EACCESS;
   }
   file_data_t* fileData = (file_data_t*)(&block.data);

   // Do not allow modification of read-only file.
   if (fileData->meta.RO) {
      return TFS_ERO;
   }

   int err;
   err = removeFileFromDir(fileAddr, dirAddr);
   if (err < 0) {
      return err;
   }
   err = deallocateFile(fileAddr);
   if (err < 0) {
      return err;
   }

   return TFS_SUCCESS;
}

int deleteDir(unsigned char dirAddr, unsigned char parentDirAddr) {
   // Get the directory block.
   block_t block;
   if (readBlock(tfsDisk, dirAddr, &block) == -1) {
      return TFS_EACCESS;
   }

   // Make sure this block is a directory.
   if (block.head.type != DIRECTORY) {
      return TFS_ETYPE;
   }
   // Make sure this directory is empty.
   dir_data_t* dirData = (dir_data_t*)(&block.data);
   if (dirData->dirMeta.nFiles > 0) {
      return TFS_ENOTEMPTY;
   }

   // Do not allow modification of read-only file.
   if (dirData->dirMeta.meta.RO) {
      return TFS_ERO;
   }

   // Delete the directory from its parent.
   int err;
   err = removeFileFromDir(dirAddr, parentDirAddr);
   if (err < 0) {
      return err;
   }
   // And deallocate its block.
   err = deallocateFile(dirAddr);
   if (err < 0) {
      return err;
   }

   return TFS_SUCCESS;
}

int addFileToDir(unsigned char fileAddr, unsigned char dirAddr) {
   block_t block;
   if (readBlock(tfsDisk, dirAddr, &block) == -1) {
      return TFS_EACCESS;
   }
   dir_data_t* dirData = (dir_data_t*)(&block.data);

   int nFiles = dirData->dirMeta.nFiles;
   dirData->fileAddrs[nFiles] = fileAddr;
   ++dirData->dirMeta.nFiles;

   if (writeBlock(tfsDisk, dirAddr, &block) == -1) {
      return TFS_EACCESS;
   }

   return TFS_SUCCESS;
}

int removeFileFromDir(unsigned char fileAddr, unsigned char dirAddr) {
   block_t block;
   if (readBlock(tfsDisk, dirAddr, &block) == -1) {
      return TFS_EACCESS;
   }
   dir_data_t* dirData = (dir_data_t*)(&block.data);

   // Find location of file to remove in file address array.
   int ndx, nFiles = dirData->dirMeta.nFiles;
   for (ndx = 0; ndx < nFiles; ++ndx) {
      if (dirData->fileAddrs[ndx] == fileAddr) {
         break;
      }
   }

   // File not in array.
   if (ndx == nFiles) {
      return TFS_ENOTFOUND;
   }

   // Shift all other files down.
   for (; ndx < nFiles - 1; ++ndx) {
      dirData->fileAddrs[ndx] = dirData->fileAddrs[ndx + 1];
   }

   --dirData->dirMeta.nFiles;

   if (writeBlock(tfsDisk, dirAddr, &block) == -1) {
      return TFS_EACCESS;
   }

   return TFS_SUCCESS;
}

int removeAllHelper(unsigned char dirAddr, unsigned char parentDirAddr) {
   // Get the directory block.
   block_t dirBlock;
   if (readBlock(tfsDisk, dirAddr, &dirBlock) == -1) {
      return TFS_EACCESS;
   }
   dir_data_t* dirData = (dir_data_t*)(&dirBlock.data);

   if (dirBlock.head.type == INODE) {
      return deleteFile(dirAddr, parentDirAddr);
   } else if (dirBlock.head.type == DIRECTORY) {
      int ndx, nFiles = dirData->dirMeta.nFiles;
      for (ndx = nFiles - 1; ndx >= 0; --ndx) {
         int err = removeAllHelper(dirData->fileAddrs[ndx], dirAddr);
         if (err < 0) {
            return err;
         }
      }

      return deleteDir(dirAddr, parentDirAddr);
   } else {
      return TFS_ETYPE;
   }
}

int deallocateFile(unsigned char addr) {
   while (addr != 0) {
      block_t block;
      if (readBlock(tfsDisk, addr, &block) == -1) {
         return TFS_EACCESS;
      }

      unsigned char nextAddr = block.head.addr;
      addBlockToFreeList(addr);
      addr = nextAddr;
   }

   return TFS_SUCCESS;
}

int addBlockToFreeList(unsigned char addr) {
   block_t superBlock;
   if (readBlock(tfsDisk, SUPER_BLOCK_ADDR, &superBlock) == -1) {
      return TFS_EACCESS;
   }
   super_data_t* superData = (super_data_t*)(&superBlock.data);

   block_t freeBlock;
   if (readBlock(tfsDisk, addr, &freeBlock) == -1) {
      return TFS_EACCESS;
   }

   initBlock(&freeBlock, FREE, superData->freeAddr);
   if (writeBlock(tfsDisk, addr, &freeBlock) == -1) {
      return TFS_EACCESS;
   }

   superData->freeAddr = addr;
   if (writeBlock(tfsDisk, SUPER_BLOCK_ADDR, &superBlock) == -1) {
      return TFS_EACCESS;
   }

   return TFS_SUCCESS;
}

fileDescriptor addFileToTable(unsigned char addr, unsigned char dirAddr) {
   fd_row_t* fdRow;
   int ndx;

   // Look for an empty slot before appending to the table.
   for (ndx = 0; ndx < fileTableSize; ++ndx) {
      fdRow = fileTable + ndx;
      // Empty slot found.
      if (fdRow->addr == 0) {
         fdRow->addr = addr;
         fdRow->dirAddr = dirAddr;
         fdRow->cursor = 0;
         return (fileDescriptor)ndx;
      }
   }

   // No empty slots, and not enough room in the table.
   if (fileTableSize >= fileTableLength) {
      fileTableLength *= 2;
      int size = fileTableLength * sizeof(fd_row_t);
      fileTable = (fd_row_t*)realloc(fileTable, size);
   }

   fdRow = fileTable + fileTableSize;
   fdRow->addr = addr;
   fdRow->dirAddr = dirAddr;
   fdRow->cursor = 0;

   return fileTableSize++;
}

char isActiveFD(fileDescriptor FD) {
   return FD >= 0 && FD < fileTableSize && fileTable[FD].addr != 0;
}

char isValidName(char* name) {
   if (name == NULL) {
      return 0;
   }

   int len = strnlen(name, MAX_FILE_NAME_LENGTH);
   if (len == 0 || len >= MAX_FILE_NAME_LENGTH - 1) {
      return 0;
   }

   if (strpbrk(name, "~!@#$%^&*()=[]{}\\|;:'\"<>?/") != NULL) {
      return 0;
   }

   return 1;
}

int getFDBlock(fileDescriptor FD, block_t* block) {
   if (!isActiveFD(FD)) {
      return TFS_EBADFD;
   }

   unsigned char fileAddr = fileTable[FD].addr;
   if (readBlock(tfsDisk, fileAddr, block) == -1) {
      return TFS_EACCESS;
   }

   return TFS_SUCCESS;
}

int getFileInode(fileDescriptor FD, file_data_t* fileData) {
   block_t block;
   int err = getFDBlock(FD, &block);
   if (err != TFS_SUCCESS) {
      return err;
   }

   if (block.head.type != INODE) {
      return TFS_ETYPE;
   }

   memcpy(fileData, block.data, sizeof(file_data_t));

   return TFS_SUCCESS;
}

int listDiskHelper(unsigned char dirAddr, int indent) {
   block_t dirBlock;
   if (readBlock(tfsDisk, dirAddr, &dirBlock) == -1) {
      return TFS_EACCESS;
   }
   dir_data_t* dirData = (dir_data_t*)(&dirBlock.data);

   int ndx, nFiles;
   nFiles = dirData->dirMeta.nFiles;
   for (ndx = 0; ndx < nFiles; ++ndx) {
      unsigned char fileAddr = dirData->fileAddrs[ndx];

      block_t fileBlock;
      if (readBlock(tfsDisk, fileAddr, &fileBlock) == -1) {
         return TFS_EACCESS;
      }
      file_data_t* fileData = (file_data_t*)(&fileBlock.data);

      printf("%*s%s\n", indent * 3, "", fileData->meta.name);

      if (fileBlock.head.type == DIRECTORY) {
         int err = listDiskHelper(fileAddr, indent + 1);
         if (err < 0) {
            return err;
         }
      }
   }

   return TFS_SUCCESS;
}

int listDirContents(dir_data_t* dirData, int indent) {
   int ndx, nFiles;
   nFiles = dirData->dirMeta.nFiles;
   for (ndx = 0; ndx < nFiles; ++ndx) {
      block_t block;
      if (readBlock(tfsDisk, dirData->fileAddrs[ndx], &block) == -1) {
         return TFS_EACCESS;
      }
      file_data_t* fileData = (file_data_t*)(&block.data);
      printf("%*s%s\n", indent * 3, "", fileData->meta.name);
   }

   return TFS_SUCCESS;
}

int writeFileData(char* buffer, int nBytes) {
   block_t block;
   unsigned char headDataAddr, dataAddr;

   int ndx, nBlocks = bytesToBlocks(nBytes);
   headDataAddr = dataAddr = requisitionBlock();
   for (ndx = 0; ndx < nBlocks - 1; ++ndx) {
      if (dataAddr < 0) {
         return dataAddr;
      }

      int nextDataAddr = requisitionBlock();
      if (nextDataAddr < 0) {
         return dataAddr;
      }

      initDataBlock(&block);
      block.head.addr = nextDataAddr;
      memcpy(block.data, buffer + (ndx * BLOCK_DATA_SIZE), BLOCK_DATA_SIZE);
      if (writeBlock(tfsDisk, dataAddr, &block) == -1) {
         return TFS_EACCESS;
      }

      dataAddr = nextDataAddr;
   }

   if (dataAddr < 0) {
      return dataAddr;
   }

   int finalBufferSize = ((nBytes - 1) % BLOCK_DATA_SIZE) + 1;
   initDataBlock(&block);
   memcpy(block.data, buffer + (ndx * BLOCK_DATA_SIZE), finalBufferSize);
   if (writeBlock(tfsDisk, dataAddr, &block) == -1) {
      return TFS_EACCESS;
   }

   return headDataAddr;
}

int requisitionBlockList(int nBlocks) {
   // Fetch super block.
   block_t superBlock;
   if (readBlock(tfsDisk, SUPER_BLOCK_ADDR, &superBlock) == -1) {
      return TFS_EACCESS;
   }
   super_data_t* superData = (super_data_t*)(&superBlock.data);
   if (superData->nFreeBlocks < nBlocks) {
      return TFS_ESPACE;
   }

   int headDataAddr, dataAddr;
   headDataAddr = dataAddr = requisitionBlock();
   if (dataAddr < 0) {
      return dataAddr;
   }

   block_t dataBlock;
   int ndx;
   for (ndx = 0; ndx < nBlocks - 1; ++ndx) {
      if (readBlock(tfsDisk, dataAddr, &dataBlock) == -1) {
         return TFS_EACCESS;
      }

      dataBlock.head.addr = requisitionBlock();
      if (writeBlock(tfsDisk, dataAddr, &dataBlock) == -1) {
         return TFS_EACCESS;
      }

      dataAddr = dataBlock.head.addr;
   }

   if (readBlock(tfsDisk, dataAddr, &dataBlock) == -1) {
      return TFS_EACCESS;
   }

   dataBlock.head.addr = 0;
   if (writeBlock(tfsDisk, dataAddr, &dataBlock) == -1) {
      return TFS_EACCESS;
   }

   return headDataAddr;
}

int requisitionBlock() {
   // Fetch super block.
   block_t superBlock;
   if (readBlock(tfsDisk, SUPER_BLOCK_ADDR, &superBlock) == -1) {
      return TFS_EACCESS;
   }
   super_data_t* superData = (super_data_t*)(&superBlock.data);

   // Find current free-list head.
   unsigned char victimAddr = superData->freeAddr;
   if (victimAddr == 0) {
      return TFS_ESPACE;
   }

   // Fetch current free-list head.
   block_t victimBlock;
   if (readBlock(tfsDisk, victimAddr, &victimBlock) == -1) {
      return TFS_EACCESS;
   }

   // Find next block in free-list.
   unsigned char freeAddr = victimBlock.head.addr;

   // Update free-list head pointer.
   superData->freeAddr = freeAddr;
   if (writeBlock(tfsDisk, SUPER_BLOCK_ADDR, &superBlock) == -1) {
      return TFS_EACCESS;
   }

   return victimAddr;
}

int getDataBlockAtOffset(unsigned char addr, unsigned int offset) {
   int ndx, bNum = offset / BLOCK_DATA_SIZE;

   for (ndx = 0; ndx < bNum; ++ndx) {
      block_t block;
      if (readBlock(tfsDisk, addr, &block) == -1) {
         return TFS_EACCESS;
      }

      addr = block.head.addr;
   }

   return addr;
}

void printFileTable() {
   printf("file table\n");
   int ndx = 0;
   for (ndx = 0; ndx < fileTableSize; ++ndx) {
      printf("%d:\n", ndx);
      printf("   addr:%u\n", fileTable[ndx].addr);
      printf("   dirAddr:%u\n", fileTable[ndx].dirAddr);
      printf("   cursor%u\n", fileTable[ndx].cursor);
   }
}

void printBlock(unsigned char addr) {
   block_t block;
   if (readBlock(tfsDisk, addr, &block) == -1) {
      printf("could not read block %d\n", addr);
      return;
   }

   printf("block addr: %d\n", addr);
   printf("block head:\n");
   printf("type:  %x\n", block.head.type);
   printf("magic: %x\n", block.head.magic);
   printf("addr:  %x\n", block.head.addr);
   printf("empty: %x\n", block.head.empty);

   if (block.head.type == SUPER) {
      printSuperBlock((super_data_t*)(&block.data));
   } else if (block.head.type == DIRECTORY) {
      printDirectoryBlock((dir_data_t*)(&block.data));
   } else if (block.head.type == INODE) {
      printInodeBlock((file_data_t*)(&block.data));
   } else if (block.head.type == DATA) {
   } else if (block.head.type == FREE) {
   } else {
   }
}

void printSuperBlock(super_data_t* superData) {
   printf("nBlocks:  %u\n", superData->nBlocks);
   printf("rootAddr: %u\n", superData->rootAddr);
   printf("dataAddr: %u\n", superData->freeAddr);
}

void printDirectoryBlock(dir_data_t* dirData) {
   printMetaData(&dirData->dirMeta.meta);
   printf("nFiles:  %u\n", dirData->dirMeta.nFiles);
   int ndx;
   for (ndx = 0; ndx < dirData->dirMeta.nFiles; ++ndx) {
      printf("   %u\n", dirData->fileAddrs[ndx]);
   }
}

void printInodeBlock(file_data_t* fileData) {
   printMetaData(&fileData->meta);
   printf("size:  %u\n", fileData->size);
}

void printMetaData(meta_data_t* metaData) {
   printf("RO:    %d\n", metaData->RO);
   printf("cTime: %llu\n", (unsigned long long)metaData->cTime);
   printf("mTime: %llu\n", (unsigned long long)metaData->mTime);
   printf("aTime: %llu\n", (unsigned long long)metaData->aTime);
   printf("name:  %s\n", metaData->name);
}

