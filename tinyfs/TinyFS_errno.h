#ifndef __TINY_FS_ERRNO_H__
#define __TINY_FS_ERRNO_H__

#define TFS_SUCCESS       0
#define TFS_EACCESS      -1
#define TFS_ESIZE        -2
#define TFS_EONEFS       -3
#define TFS_ENOFS        -4
#define TFS_ENOTFS       -5
#define TFS_ESPACE       -6
#define TFS_ENAME        -7
#define TFS_ETYPE        -8
#define TFS_EEXISTS      -9
#define TFS_ENOTFOUND   -10
#define TFS_ENOTEMPTY   -11
#define TFS_EBADFD      -12
#define TFS_EDATA       -13
#define TFS_ERO         -14
#define TFS_ERW         -15
#define TFS_EUNKNOWN    -16

#endif /* __TINY_FS_ERRNO_H__ */

