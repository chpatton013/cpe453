#include "libTinyFS.h"
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define FILE_NOT_EXISTS "fileNotExists"
#define FILE_EXISTS "fileExists"
#define FILE_VALID "fileValid"
#define FILE_INVALID "fileInvalid"

#define TEST(func) \
   do { \
      setup(); \
      func(); \
      teardown(); \
   } while(0)

#define DTEST(func) \
   do { \
      dfltSetup(); \
      func(); \
      dfltTeardown(); \
   } while(0)

#define pHead(...) \
   do { \
      printf("\n================\n"); \
      printf(__VA_ARGS__); \
      printf("\n================\n"); \
   } while(0)

#define pSub(...) printf("   " __VA_ARGS__)

#define pTail(...) printf("================\n")

#define DEMO(expr, expect) \
   do { \
      int exprVal = (expr); \
      int expeVal = (expect); \
      pSub("Expecting: %s (%s)\n", getLabel(expeVal), getMessage(expeVal)); \
      if (expect == exprVal) { \
         pSub("Actual:    %s\n", getLabel(exprVal)); \
      } else { \
         pSub("Actual:    %s (%s)\n", getLabel(exprVal), getMessage(exprVal)); \
      } \
      pSub("\n"); \
   } while(0)

#define VALUE(x) ('A' + (((x) / 63) % 26))

static void setup();
static void teardown();

static void dfltSetup();
static void dfltTeardown();

static void dFormat();
static void dMount();
static void dUnmount();
static void dMountUnmount();

static void dFileCreationDeletion();
static void dFileOpenClose();
static void dBulkWriteRead();
static void dSeek();

static void dIncrementalWriteRead();
static void dRename();
static void dDirCreationDeletion();
static void dNestedDirCreationDeletion();
static void dFileReadOnly();
static void dDirReadOnly();
static void dTimestamps();

static void demoWriteFile(fileDescriptor FD, int size);
static char* initBuffer(int size);
static void compareFileToBuffer(fileDescriptor FD, char* buffer, int size);
static void printInLoop(int ndx, char byte);
static void printTimestamps(time_t c, time_t m, time_t a);
static void printTimestampsFromFile(fileDescriptor FD);

static const char* getLabel(int error);
static const char* getMessage(int error);

int main(int argc, char** argv) {
   printf("TinyFS Demo Driver\n\n");

   printf("+=======================================\n");
   printf("| Demonstrating core functionality:\n");
   printf("|    tfs_mkfs\n");
   printf("|    tfs_mount\n");
   printf("|    tfs_unmount\n");
   printf("|    tfs_openFile\n");
   printf("|    tfs_deleteFile\n");
   printf("|    tfs_closeFile\n");
   printf("|    tfs_writeFile\n");
   printf("|    tfs_readByte\n");
   printf("|    tfs_seek\n");
   printf("+=======================================\n");

   TEST(dFormat);
   TEST(dMount);
   TEST(dUnmount);
   TEST(dMountUnmount);

   DTEST(dFileCreationDeletion);
   DTEST(dFileOpenClose);
   DTEST(dBulkWriteRead);
   DTEST(dSeek);

   printf("+=======================================\n");
   printf("| Demonstrating extra functionality:\n");
   printf("|    tfs_writeByte\n");
   printf("|    tfs_rename\n");
   printf("|    tfs_createDir\n");
   printf("|    tfs_removeDir\n");
   printf("|    tfs_removeAll\n");
   printf("|    tfs_makeRO\n");
   printf("|    tfs_makeRW\n");
   printf("|    tfs_readFileInfo (tfs_getCTime, tfs_getMTime, tfs_getATime)\n");
   printf("+=======================================\n");

   DTEST(dIncrementalWriteRead);
   DTEST(dRename);
   DTEST(dDirCreationDeletion);
   DTEST(dNestedDirCreationDeletion);
   DTEST(dFileReadOnly);
   DTEST(dDirReadOnly);
   DTEST(dTimestamps);

   return 0;
}

void setup() {
   // FILE_NOT_EXISTS
   unlink(FILE_NOT_EXISTS);

   // FILE_EXISTS
   unlink(FILE_EXISTS);
   int existsFd = open(FILE_EXISTS, O_RDWR | O_CREAT, (mode_t)0664);
   lseek(existsFd, DEFAULT_DISK_SIZE, SEEK_SET);
   close(existsFd);

   // FILE_VALID
   unlink(FILE_VALID);
   int validFd = open(FILE_VALID, O_RDWR | O_CREAT, (mode_t)0664);
   int ndx;
   for (ndx = 0; ndx < DEFAULT_DISK_SIZE / BLOCKSIZE; ++ndx) {
      char validBlock[BLOCKSIZE] = {0};
      validBlock[1] = 0x45;
      write(validFd, &validBlock, BLOCKSIZE);
   }
   close(validFd);

   // FILE_INVALID
   unlink(FILE_INVALID);
   int invalidFd = open(FILE_INVALID, O_RDWR | O_CREAT, (mode_t)0664);
   char invalidBuffer[DEFAULT_DISK_SIZE] = {0};
   write(invalidFd, invalidBuffer, DEFAULT_DISK_SIZE);
   close(invalidFd);

   // DEFAULT_DISK
   unlink(DEFAULT_DISK_NAME);
   int defaultFd = open(DEFAULT_DISK_NAME, O_RDWR | O_CREAT, (mode_t)0664);
   lseek(defaultFd, DEFAULT_DISK_SIZE, SEEK_SET);
   close(defaultFd);
}

void teardown() {
   unlink(FILE_NOT_EXISTS);
   unlink(FILE_EXISTS);
   unlink(FILE_VALID);
   unlink(FILE_INVALID);
   unlink(DEFAULT_DISK_NAME);
}

void dfltSetup() {
   unlink(DEFAULT_DISK_NAME);
   int defaultFd = open(DEFAULT_DISK_NAME, O_RDWR | O_CREAT, (mode_t)0664);
   lseek(defaultFd, DEFAULT_DISK_SIZE, SEEK_SET);
   close(defaultFd);

   tfs_mkfs(DEFAULT_DISK_NAME, DEFAULT_DISK_SIZE);
   tfs_mount(DEFAULT_DISK_NAME);
}

void dfltTeardown() {
   tfs_unmount();
   unlink(DEFAULT_DISK_NAME);
}

void dFormat() {
   pHead("Formatting");

   pSub("Non-existant file, size 0\n");
   DEMO(tfs_mkfs(FILE_NOT_EXISTS, 0), TFS_ESIZE);

   pSub("Non-existant file, size 1\n");
   DEMO(tfs_mkfs(FILE_NOT_EXISTS, 1), TFS_ESIZE);

   pSub("Non-existant file, size %d\n", DEFAULT_DISK_SIZE - 1);
   DEMO(tfs_mkfs(FILE_NOT_EXISTS, DEFAULT_DISK_SIZE - 1), TFS_ESIZE);

   pSub("Non-existant file, size %d\n", DEFAULT_DISK_SIZE);
   DEMO(tfs_mkfs(FILE_NOT_EXISTS, DEFAULT_DISK_SIZE), TFS_SUCCESS);
   unlink(FILE_NOT_EXISTS);

   pSub("Existant file, size %d\n", DEFAULT_DISK_SIZE);
   DEMO(tfs_mkfs(FILE_NOT_EXISTS, DEFAULT_DISK_SIZE), TFS_SUCCESS);

   pSub("Existant file, size %d\n", DEFAULT_DISK_SIZE - 1);
   DEMO(tfs_mkfs(FILE_NOT_EXISTS, DEFAULT_DISK_SIZE - 1), TFS_ESIZE);

   pSub("Non-existant file, size 1\n");
   DEMO(tfs_mkfs(FILE_NOT_EXISTS, 1), TFS_ESIZE);

   pSub("Non-existant file, size 0\n");
   DEMO(tfs_mkfs(FILE_NOT_EXISTS, 0), TFS_SUCCESS);
   unlink(FILE_NOT_EXISTS);
}

void dMount() {
   pHead("Mounting");

   pSub("Non-existant file\n");
   DEMO(tfs_mount(FILE_NOT_EXISTS), TFS_EACCESS);

   pSub("Valid file\n");
   DEMO(tfs_mount(FILE_VALID), TFS_SUCCESS);
   tfs_unmount();

   pSub("Valid file, then non-existant file\n");
   tfs_mount(FILE_VALID);
   DEMO(tfs_mount(FILE_NOT_EXISTS), TFS_EONEFS);
   tfs_unmount();

   pSub("Valid file, then valid file\n");
   tfs_mount(FILE_VALID);
   DEMO(tfs_mount(FILE_VALID), TFS_EONEFS);
   tfs_unmount();

   pSub("Invalid file\n");
   DEMO(tfs_mount(FILE_INVALID), TFS_ENOTFS);

   pSub("Invalid file, then valid file\n");
   tfs_mount(FILE_INVALID);
   DEMO(tfs_mount(FILE_VALID), TFS_SUCCESS);
   tfs_unmount();
}

void dUnmount() {
   pHead("Unmounting");

   pSub("No file mounted\n");
   DEMO(tfs_unmount(), TFS_ENOFS);

   pSub("Mounting non-existant file, then unmounting\n");
   tfs_mount(FILE_NOT_EXISTS);
   DEMO(tfs_unmount(), TFS_ENOFS);

   pSub("Mounting invalid file, then unmounting\n");
   tfs_mount(FILE_INVALID);
   DEMO(tfs_unmount(), TFS_ENOFS);

   pSub("Mounting valid file, then unmounting\n");
   tfs_mount(FILE_VALID);
   DEMO(tfs_unmount(), TFS_SUCCESS);
}

void dMountUnmount() {
   pHead("Mounting and Unmounting");

   printf("Mount valid file, unmount, mount non-existant file, then unmount\n");
   tfs_mount(FILE_VALID);
   tfs_unmount();
   tfs_mount(FILE_NOT_EXISTS);
   DEMO(tfs_unmount(), TFS_ENOFS);

   printf("Mount valid file, unmount, mount invalid file, then unmount\n");
   tfs_mount(FILE_VALID);
   tfs_unmount();
   tfs_mount(FILE_INVALID);
   DEMO(tfs_unmount(), TFS_ENOFS);

   printf("Mount valid file, unmount, mount valid file, then unmount\n");
   tfs_mount(FILE_VALID);
   tfs_unmount();
   tfs_mount(FILE_VALID);
   DEMO(tfs_unmount(), TFS_SUCCESS);
}

void dFileCreationDeletion() {
   pHead("File Creation and Deletion");

   pSub("Creating files 'a', 'b', and 'c'\n");
   fileDescriptor a, b, c;
   a = tfs_openFile("a");
   b = tfs_openFile("b");
   c = tfs_openFile("c");

   pSub("Disk contents should be:\na\nb\nc\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");

   pSub("Deleting files 'a', 'b', and 'c'\n");
   tfs_deleteFile(a);
   tfs_deleteFile(b);
   tfs_deleteFile(c);

   pSub("Disk should be empty\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");
}

void dFileOpenClose() {
   pHead("File Opening and Closing");

   pSub("Closing with invalid file descriptor\n");
   DEMO(tfs_closeFile(0), TFS_EBADFD);

   pSub("Creating files 'a', 'b', and 'c'\n");
   fileDescriptor a, b, c;
   a = tfs_openFile("a");
   b = tfs_openFile("b");
   c = tfs_openFile("c");

   pSub("File descriptors should be:\n0\n1\n2\n");
   pSub("Printing file descriptors:\n%d\n%d\n%d\n", a, b, c);

   pSub("Closing 'b', then writing to 'b'\n");
   tfs_closeFile(b);
   char buffer[100] = {0};
   DEMO(tfs_writeFile(b, buffer, 100), TFS_EBADFD);

   pSub("Opening 'd'\n");
   fileDescriptor d = tfs_openFile("d");

   pSub("File descriptor for 'd' should be:\n1\n");
   pSub("Printing file descriptor:\n%d\n", d);
}

void dBulkWriteRead() {
   pHead("Bulk Write and Incremental Read");

   fileDescriptor FD = tfs_openFile("a");

   demoWriteFile(FD, 0);
   demoWriteFile(FD, 1);
   demoWriteFile(FD, 252);
   demoWriteFile(FD, 300);
   demoWriteFile(FD, 600);

   pSub("Writing huge buffer to disk\n");
   char hugeBuffer[DEFAULT_DISK_SIZE * 252];
   DEMO(tfs_writeFile(FD, hugeBuffer, DEFAULT_DISK_SIZE * 252), TFS_ESPACE);
}

void dSeek() {
   pHead("Seek");

   fileDescriptor FD = tfs_openFile("a");

   char* buffer = initBuffer(300);
   pSub("Writing %d byte buffer to file\n", 300);
   DEMO(tfs_writeFile(FD, buffer, 300), TFS_SUCCESS);
   compareFileToBuffer(FD, buffer, 300);

   pSub("Seeking to middle 100 bytes of file\n");
   DEMO(tfs_seek(FD, 100), TFS_SUCCESS);
   pSub("Comparing middle 100 bytes of file to buffer\n");
   compareFileToBuffer(FD, buffer + 100, 100);

   pSub("Seeking to first 100 bytes of file\n");
   DEMO(tfs_seek(FD, 0), TFS_SUCCESS);
   pSub("Comparing first 100 bytes of file to buffer\n");
   compareFileToBuffer(FD, buffer, 100);

   pSub("Seeking to last 100 bytes of file\n");
   DEMO(tfs_seek(FD, 200), TFS_SUCCESS);
   pSub("Comparing last 100 bytes of file to buffer\n");
   compareFileToBuffer(FD, buffer + 200, 100);

   free(buffer);

   char empty[300] = {0};
   pSub("Seeking 300 bytes beyond EOF\n");
   DEMO(tfs_seek(FD, 600), TFS_SUCCESS);
   pSub("Seeking to the start of the 300 byte file hole\n");
   DEMO(tfs_seek(FD, 300), TFS_SUCCESS);
   pSub("Comparing file hole to null buffer (null-bytes are displayed as '_')\n");
   compareFileToBuffer(FD, empty, 300);
}

void dIncrementalWriteRead() {
   pHead("Incremental Write and Incremental Read");

   fileDescriptor FD = tfs_openFile("a");

   int ndx;
   int errsFound, errNdx, errVal;
   char* buffer = initBuffer(300);

   pSub("Performing 300 incremental writes\n");
   errsFound = 0;
   for (ndx = 0; ndx < 300; ++ndx) {
      int err = tfs_writeByte(FD, buffer[ndx]);
      if (err != TFS_SUCCESS) {
         ++errsFound;
         if (!errsFound) {
            errNdx = ndx;
            errVal = err;
         }
      }
   }

   if (errsFound) {
      pSub("%d errors encountered during writing! First error at %d: %s (%s)\n\n",
       errsFound, errNdx, getLabel(errVal), getMessage(errVal));
   } else {
      pSub("All writes completed successfully\n\n");
   }

   pSub("Seeking to start of file\n");
   DEMO(tfs_seek(FD, 0), TFS_SUCCESS);

   compareFileToBuffer(FD, buffer, 300);

   free(buffer);
}

void dRename() {
   pHead("File Renaming");

   pSub("Creating files 'a', 'b', and 'c'\n");
   fileDescriptor a, b, c;
   a = tfs_openFile("a");
   b = tfs_openFile("b");
   c = tfs_openFile("c");

   pSub("Disk contents should be:\na\nb\nc\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");

   pSub("Renaming 'a' to existant file 'b'\n");
   DEMO(tfs_rename(a, "b"), TFS_EEXISTS);

   pSub("Disk contents should be:\na\nb\nc\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");

   pSub("Renaming 'a' to non-existant file 'b'\n");
   DEMO(tfs_rename(a, "d"), TFS_SUCCESS);

   pSub("Disk contents should be:\nd\nb\nc\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");
}

void dDirCreationDeletion() {
   pHead("Directory Creation / Deletion");

   pSub("Creating directories 'a', 'b', and 'c'\n");
   DEMO(tfs_createDir("a"), TFS_SUCCESS);
   DEMO(tfs_createDir("b"), TFS_SUCCESS);
   DEMO(tfs_createDir("c"), TFS_SUCCESS);

   pSub("Disk contents should be:\na\nb\nc\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");

   pSub("Creating file 'a/d'\n");
   fileDescriptor FD = tfs_openFile("a/d");

   pSub("Disk contents should be:\na\n   d\nb\nc\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");

   printf("Deleting empty directories 'b' and 'c'\n");
   DEMO(tfs_removeDir("b"), TFS_SUCCESS);
   DEMO(tfs_removeDir("c"), TFS_SUCCESS);

   pSub("Disk contents should be:\na\n   d\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");

   printf("Deleting non-empty directory 'a'\n");
   DEMO(tfs_removeDir("a"), TFS_ENOTEMPTY);

   pSub("Disk contents should be:\na\n   d\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");

   pSub("Deleting file 'a/d'\n");
   tfs_deleteFile(FD);
   pSub("Deleting empty directory 'a'\n");
   DEMO(tfs_removeDir("a"), TFS_SUCCESS);

   pSub("Disk contents should be:\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");
}

void dNestedDirCreationDeletion() {
   pHead("Nested Directory Creation / Deletion");

   pSub("Creating directories 'a', 'b', and 'c'\n");
   tfs_createDir("a");
   tfs_createDir("b");
   tfs_createDir("c");

   pSub("Creating nested directories 'a/d', 'a/e', and 'a/f'\n");
   tfs_createDir("a/d");
   tfs_createDir("a/e");
   tfs_createDir("a/f");

   pSub("Creating nested files 'b/d', 'b/e', and 'b/f'\n");
   tfs_openFile("b/g");
   tfs_openFile("b/h");
   tfs_openFile("b/i");

   pSub("Creating nested directories 'a/d/j', 'a/d/k', and 'a/d/l'\n");
   tfs_createDir("a/d/j");
   tfs_createDir("a/d/k");
   tfs_createDir("a/d/l");

   pSub("Disk contents should be:\n");
   printf("a\n");
   printf("   d\n");
   printf("      j\n");
   printf("      k\n");
   printf("      l\n");
   printf("   e\n");
   printf("   f\n");
   printf("b\n");
   printf("   g\n");
   printf("   h\n");
   printf("   i\n");
   printf("c\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");

   pSub("Recursively deleting 'a/d'\n");
   DEMO(tfs_removeAll("a/d"), TFS_SUCCESS);

   pSub("Disk contents should be:\n");
   printf("a\n");
   printf("   e\n");
   printf("   f\n");
   printf("b\n");
   printf("   g\n");
   printf("   h\n");
   printf("   i\n");
   printf("c\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");

   pSub("Recursively deleting non-existant directory 'a/d'\n");
   DEMO(tfs_removeAll("a/d"), TFS_ENOTFOUND);

   pSub("Disk contents should be:\n");
   printf("a\n");
   printf("   e\n");
   printf("   f\n");
   printf("b\n");
   printf("   g\n");
   printf("   h\n");
   printf("   i\n");
   printf("c\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");

   pSub("Recursively deleting all files and directories on device\n");
   DEMO(tfs_removeAll("/"), TFS_SUCCESS);

   pSub("Disk contents should be:\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");
}

void dFileReadOnly() {
   pHead("File Read-Only");

   fileDescriptor FD = tfs_openFile("a");
   char buffer[100];

   pSub("Making non-existant file read-only\n");
   DEMO(tfs_makeRO("b"), TFS_ENOTFOUND);

   pSub("Making existant file read-only\n");
   DEMO(tfs_makeRO("a"), TFS_SUCCESS);

   pSub("Modifying file with tfs_writeFile\n");
   DEMO(tfs_writeFile(FD, buffer, 100), TFS_ERO);

   pSub("Modifying file with tfs_writeByte\n");
   DEMO(tfs_writeByte(FD, 0), TFS_ERO);

   pSub("Modifying file with tfs_seek\n");
   DEMO(tfs_seek(FD, 200), TFS_ERO);

   pSub("Renaming file to 'b'\n");
   DEMO(tfs_rename(FD, "b"), TFS_ERO);

   pSub("Disk contents should be:\na\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");
}

void dDirReadOnly() {
   pHead("Directory Read-Only");

   pSub("Creating directory 'a'\n");
   tfs_createDir("a");

   pSub("Creating file 'a/b'");
   fileDescriptor FD = tfs_openFile("a/b");

   pSub("Making file 'a/b' read-only\n");
   DEMO(tfs_makeRO("a/b"), TFS_SUCCESS);

   pSub("Disk contents should be:\na\n   b\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");

   pSub("Recursively deleting all files and directories on device\n");
   DEMO(tfs_removeAll("/"), TFS_ERO);

   pSub("Disk contents should be:\na\n   b\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");

   pSub("Making file 'a/b' read-write\n");
   DEMO(tfs_makeRW("a/b"), TFS_SUCCESS);

   pSub("Deleting file 'a/b'\n");
   DEMO(tfs_deleteFile(FD), TFS_SUCCESS);

   pSub("Disk contents should be:\na\n");
   pSub("Listing disk contents:\n");
   tfs_listDisk();
   pSub("\n");

   pSub("Making directory 'a' read-only\n");
   DEMO(tfs_makeRO("a"), TFS_SUCCESS);

   pSub("Deleting directory 'a'\n");
   DEMO(tfs_removeDir("a"), TFS_ERO);
}

void dTimestamps() {
   pHead("Timestamps");

   time_t c, m, a;

   c = m = a = time(NULL);
   fileDescriptor FD = tfs_openFile("a");

   pSub("Timestamps for file 'a' should be:\n");
   printTimestamps(c, m, a);
   pSub("Printing timestamps for file 'a':\n");
   printTimestampsFromFile(FD);
   pSub("\n");

   pSub("Sleeping for 1 second\n");
   sleep(1);
   pSub("\n");

   m = time(NULL);

   pSub("Writing 1 byte to file 'a'");
   tfs_writeByte(FD, 0);

   pSub("Timestamps for file 'a' should be:\n");
   printTimestamps(c, m, a);
   pSub("Printing timestamps for file 'a':\n");
   printTimestampsFromFile(FD);
   pSub("\n");

   pSub("Sleeping for 1 second\n");
   sleep(1);
   pSub("\n");

   a = time(NULL);

   pSub("Reading 1 byte from file 'a'");
   char byte;
   tfs_seek(FD, 0);
   tfs_readByte(FD, &byte);

   pSub("Timestamps for file 'a' should be:\n");
   printTimestamps(c, m, a);
   pSub("Printing timestamps for file 'a':\n");
   printTimestampsFromFile(FD);
   pSub("\n");
}

void demoWriteFile(fileDescriptor FD, int size) {
   char* buffer = initBuffer(size);
   pSub("Writing %d byte buffer to file\n", size);
   DEMO(tfs_writeFile(FD, buffer, size), TFS_SUCCESS);
   pSub("Comparing file to buffer\n");
   compareFileToBuffer(FD, buffer, size);
   free(buffer);
}

char* initBuffer(int size) {
   char* buffer = (char*)malloc(size * sizeof(char));
   int ndx;
   for (ndx = 0; ndx < size; ++ndx) {
      buffer[ndx] = VALUE(ndx);
   }
   return buffer;
}

void compareFileToBuffer(fileDescriptor FD, char* buffer, int size) {
   int ndx;

   pSub("Buffer contents:\n");
   for (ndx = 0; ndx < size; ++ndx) {
      printInLoop(ndx, buffer[ndx]);
   }
   printf("\n");

   int diffFound = 0, diffNdx;
   char diffVal;
   pSub("File contents:\n");
   for (ndx = 0; ndx < size; ++ndx) {
      char byte;
      tfs_readByte(FD, &byte);
      printInLoop(ndx, byte);

      if (byte != buffer[ndx]) {
         ++diffFound;
         if (!diffFound) {
            diffNdx = ndx;
            diffVal = byte;
         }
      }
   }
   printf("\n");

   pSub("Verdict: ");
   if (diffFound) {
      printf("INCONSISTENT! %d differences found. "
       "First difference at index %d (%c != %c)\n\n",
       diffFound, diffNdx, buffer[diffNdx], diffVal);
   } else {
      printf("CONSISTENT\n\n");
   }
}

void printInLoop(int ndx, char byte) {
   if (ndx != 0 && ndx % 252 == 0) {
      printf("\n");
   }
   if (ndx != 0 && ndx % 63 == 0) {
      printf("\n");
   }
   putchar(byte ? byte : '_');
}

void printTimestamps(time_t c, time_t m, time_t a) {
   printf("C:%lu\n", c);
   printf("M:%lu\n", m);
   printf("A:%lu\n", a);
}

void printTimestampsFromFile(fileDescriptor FD) {
   printTimestamps(tfs_getCTime(FD), tfs_getMTime(FD), tfs_getATime(FD));
}

const char* getLabel(int error) {
   switch (error) {
   case TFS_SUCCESS:    return "TFS_SUCCESS";
   case TFS_EACCESS:    return "TFS_EACCESS";
   case TFS_ESIZE:      return "TFS_ESIZE";
   case TFS_EONEFS:     return "TFS_EONEFS";
   case TFS_ENOFS:      return "TFS_ENOFS";
   case TFS_ENOTFS:     return "TFS_ENOTFS";
   case TFS_ESPACE:     return "TFS_ESPACE";
   case TFS_ENAME:      return "TFS_ENAME";
   case TFS_ETYPE:      return "TFS_ETYPE";
   case TFS_EEXISTS:    return "TFS_EEXISTS";
   case TFS_ENOTFOUND:  return "TFS_ENOTFOUND";
   case TFS_ENOTEMPTY:  return "TFS_ENOTEMPTY";
   case TFS_EBADFD:     return "TFS_EBADFD";
   case TFS_EDATA:      return "TFS_EDATA";
   case TFS_ERO:        return "TFS_ERO";
   case TFS_ERW:        return "TFS_ERW";
   case TFS_EUNKNOWN:   return "TFS_EUNKNOWN";
   default:             return "TFS_EUNKNOWN";
   }
}

const char* getMessage(int error) {
   if (error > 0) {
      return "No error";
   }

   switch (error) {
   case TFS_SUCCESS:    return "Success";
   case TFS_EACCESS:    return "Unix FS Access Error";
   case TFS_ESIZE:      return "Incompatible TinyFS size";
   case TFS_EONEFS:     return "Only one TinyFS instance may be mounted at a time";
   case TFS_ENOFS:      return "No TinyFS instance is currently mounted";
   case TFS_ENOTFS:     return "File is not a TinyFS instance";
   case TFS_ESPACE:     return "Insufficient space";
   case TFS_ENAME:      return "Invalid name";
   case TFS_ETYPE:      return "Incorrect block type";
   case TFS_EEXISTS:    return "File or directory with that name already exists";
   case TFS_ENOTFOUND:  return "File or directory not found";
   case TFS_ENOTEMPTY:  return "Directory not empty";
   case TFS_EBADFD:     return "Invalid file descriptor";
   case TFS_EDATA:      return "No more data";
   case TFS_ERO:        return "File is read-only";
   case TFS_ERW:        return "File is read-write";
   case TFS_EUNKNOWN:   return "Unknown error";
   default:             return "Error not found";
   }
}

