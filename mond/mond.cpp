#include "mond.hpp"

#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <ctime>
#include <fstream>
#include <map>
#include <sstream>
#include <string>

void usage() {
   fprintf(stderr, "usage: mond [-s] <executable> <interval> <logFilename>\n");
   exit(1);
}

void parse(parse_reason reason) {
   switch (reason) {
   case INTERVAL:
      fprintf(stderr, "mond: interval must be an integer greater than or equal to 500\n");
      exit(2);
   default:
      fprintf(stderr, "mond: unknown error\n");
      exit(2);
   }
}

pid_t spawnChild(char* executable) {
   pid_t child = fork();
   if (child == 0) {
      raise(SIGSTOP);
      execlp(executable, executable, NULL);
      exit(3);
   }

   // Make sure child has stopped before trying to resume it.
   waitpid(child, NULL, WUNTRACED);
}

proc_stat getSysStat() {
   std::ifstream infile("/proc/stat");
   std::string line;

   long unsigned int utime;
   long unsigned int nice;
   long unsigned int stime;
   long unsigned int idletime;
   long unsigned int iowait;
   long unsigned int irq;
   long unsigned int softirq;
   long unsigned int intr;
   long unsigned int ctxt;
   unsigned int processes;
   unsigned int procs_running;
   unsigned int procs_blocked;

   while (std::getline(infile, line)) {
      std::istringstream iss(line);
      std::string label;

      iss >> label;
      if (label == "cpu") {
         iss >> utime >> nice >> stime >> idletime >> iowait >> irq >> softirq;
      } else if (label == "intr") {
         iss >> intr;
      } else if (label == "ctxt") {
         iss >> ctxt;
      } else if (label == "processes") {
         iss >> processes;
      } else if (label == "procs_running") {
         iss >> procs_running;
      } else if (label == "procs_blocked") {
         iss >> procs_blocked;
      }
   }

   return {
      utime, stime, idletime, iowait,
      irq, softirq,
      intr, ctxt,
      processes, procs_running, procs_blocked
   };
}

proc_meminfo getSysMeminfo() {
   std::ifstream infile("/proc/meminfo");
   std::string line;
   std::map< std::string, long unsigned int > index;
   while (std::getline(infile, line)) {
      std::istringstream iss(line);
      std::string label;
      long unsigned int value;

      iss >> label >> value;

      index[label] = value;
   }

   return {
      index["MemTotal:"],
      index["MemFree:"],
      index["Cached:"],
      index["SwapCached:"],
      index["Active:"],
      index["Inactive:"]
   };
}

proc_loadavg getSysLoadavg() {
   proc_loadavg st;

   FILE* fp = fopen("/proc/loadavg", "r");

   fscanf(fp, "%f %f %f", &st.one, &st.five, &st.fifteen);

   fclose(fp);

   return st;
}

proc_diskstats getSysDiskstats() {
   std::ifstream infile("/proc/meminfo");
   std::string line;
   std::map< std::string, long unsigned int > index;
   while (std::getline(infile, line)) {
      std::istringstream iss(line);
      int major, minor;
      std::string name;
      long unsigned int numReads, readMerged, readSectors, readTime;
      long unsigned int numWrites, writeMerged, writeSectors, writeTime;

      iss >> major >> minor >> name >>
       numReads >> readMerged >> readSectors >> readTime >>
       numWrites >> writeMerged >> writeSectors >> writeTime;

      if (name == "sda") {
         return {
            numReads, readSectors, readTime,
            numWrites, writeSectors, writeTime
         };
      }
   }

   return {};
}

system_status getSystemStatus() {
   return {getSysStat(), getSysMeminfo(), getSysLoadavg(), getSysDiskstats()};
}

proc_pid_stat getProcStat(pid_t pid) {
   proc_pid_stat st;

   char file[20];
   sprintf(file, "/proc/%d/stat", pid);
   FILE* fp = fopen(file, "r");

   fscanf(fp,
    "%d %s %c %d %d %d "
    "%d %d %u "
    "%lu %lu %lu %lu "
    "%lu %lu %ld %ld "
    "%ld %ld %ld "
    "%ld %llu "
    "%lu %ld %lu",
    &st.pid, st.comm, &st.state, &st.ppid, &st.pgrp, &st.session,
    &st.tty_nr, &st.tpgid, &st.flags,
    &st.minflt, &st.cminflt, &st.majflt, &st.cmajflt,
    &st.utime, &st.stime, &st.cutime, &st.cstime,
    &st.priority, &st.nice, &st.num_threads,
    &st.itrealvalue, &st.starttime,
    &st.vsize, &st.rss, &st.rsslim);

   fclose(fp);

   return st;
}

proc_pid_statm getProcStatm(pid_t pid) {
   proc_pid_statm st;

   char file[20];
   sprintf(file, "/proc/%d/statm", pid);
   FILE* fp = fopen(file, "r");

   fscanf(fp, "%d %d %d %d %d %d",
    &st.size, &st.rss, &st.share, &st.text, &st.lib, &st.data);

   fclose(fp);

   return st;
}

process_status getProcessStatus(pid_t pid) {
   return {getProcStat(pid), getProcStatm(pid)};
}

void writeTime(FILE* fp) {
   time_t t = time(0);
   struct tm* now = localtime(&t);
   char timeStr[30];
   strftime(timeStr, 30, "%a %b %d %H:%M:%S %Y", now);
   fprintf(fp, "[%s] ", timeStr);
}

void writeSystemStatus(FILE* fp, system_status& st) {
   writeTime(fp);

   fprintf(fp, "System  "
    "[PROCESS] "
    "cpuusermode %lu cpusystemmode %lu "
    "idletaskrunning %lu iowaittime %lu "
    "irqservicetime %lu softirqservicetime %lu "
    "intr %lu ctxt %lu "
    "forks %u runnable %u blocked %u "

    "[MEMORY] "
    "memtotal %lu memfree %lu "
    "cached %lu swapcached %lu "
    "active %lu inactive %lu "

    "[LOADAVG] "
    "1min %.6f 5min %.6f 15min %.6f "

    "[DISKSTATS(sda)] "
    "totalnoreads %lu totalsectorsread %lu nomsread %lu "
    "totalnowrites %lu nosectorswritten %lu nomswritten %lu"
    "\n",
    st.stat.utime, st.stat.stime,
    st.stat.idletime, st.stat.iowait,
    st.stat.irq, st.stat.softirq,
    st.stat.intr, st.stat.ctxt,
    st.stat.processes, st.stat.procs_running, st.stat.procs_blocked,

    st.meminfo.total, st.meminfo.free,
    st.meminfo.cached, st.meminfo.swapcached,
    st.meminfo.active, st.meminfo.inactive,

    st.loadavg.one, st.loadavg.five, st.loadavg.fifteen,

    st.diskstats.numReads, st.diskstats.readSectors, st.diskstats.readTime,
    st.diskstats.numWrites, st.diskstats.writeSectors, st.diskstats.writeTime);
}

void writeProcessStatus(FILE* fp, process_status& st) {
   writeTime(fp);

   fprintf(fp, "Process(%d)  "
    "[STAT] executable %s stat %c "
    "minorfaults %lu majorfaults %lu usermodetime %lu kernelmodetime %lu "
    "priority %ld nice %ld nothreads %ld "
    "vsize %lu rss %ld "
    "[STATM] program %d residentset %d share %d text %d data %d"
    "\n",
    st.stat.pid,
    st.stat.comm, st.stat.state,
    st.stat.minflt, st.stat.majflt, st.stat.utime, st.stat.stime,
    st.stat.priority, st.stat.nice, st.stat.num_threads,
    st.stat.vsize, st.stat.rss,
    st.statm.size, st.statm.rss, st.statm.share, st.statm.text, st.statm.data);
}

int main(int argc, char** argv) {
   bool system = false;

   if (argc < 4 || argc > 5) {
      usage();
   }

   // System flag specified.
   int argNdx = 1;
   if (!strcmp(argv[1], "-s")) {
      if (argc != 5) {
         usage();
      }

      system = true;
      argNdx = 2;
   }

   // Parse sleep interval argument.
   char* end;
   int interval = strtol(argv[argNdx + 1], &end, 10);
   if (interval == 0 && end == argv[argNdx + 1] || interval < 500) {
      parse(INTERVAL);
   }

   char* executable = argv[argNdx];
   char* logPath = argv[argNdx + 2];

   // Fork child job before opeing log file.
   pid_t child = spawnChild(executable);

   // Open / create the log file.
   FILE* logFp;
   if (!strcmp(logPath, "-")) {
      logFp = stdout;
   } else {
      logFp = fopen(logPath, "w");
   }

   // Resume child.
   kill(child, SIGCONT);
   waitpid(child, NULL, WCONTINUED);

   // Begin process monitoring.
   bool failed = true;
   usleep(interval);
   while (waitpid(child, NULL, WNOHANG) != child) {
      failed = false;

      if (system) {
         system_status sysSt = getSystemStatus();
         writeSystemStatus(logFp, sysSt);
      }

      process_status procSt = getProcessStatus(child);
      writeProcessStatus(logFp, procSt);

      usleep(interval);
   }

   if (failed) {
      fprintf(stderr, "child process %d failed to exec '%s'\n", child, executable);
   }

   if (logFp != stdout) {
      fclose(logFp);
   }

   return 0;
}

