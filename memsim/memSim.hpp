#ifndef __MEM_SIM_HPP_
#define __MEM_SIM_HPP_

#include <stdio.h>
#include <set>
#include <vector>

#define PAGE_SIZE 256
#define TLB_SIZE 16

typedef void (*pra)(int pageNum);

typedef unsigned char page_t[PAGE_SIZE];

struct page_map {
   int pageNum;
   int frameNum;
};

struct page_table_row {
   bool loaded;
   page_map mapping;
};

struct frame_table_row {
   bool active;
   unsigned long long when;
};

enum parse_error {REF_FILE, FRAMES, PRA};

void parseArgs(int argc, char** argv);
void usage();
void parse(parse_error);

void initSwap();
void teardownSwap();
void readReferences(FILE* refFile);
page_table_row* access(unsigned long ref);

void fifo(int pageNum);
void lru(int pageNum);
void swap(int pageNum, int frameNum);

void printAccess(unsigned long ref, page_table_row* row);
void printFrame(int frameNum);

static FILE* refFile;
static int numFrames = 256;
static int numPages = 256;
static pra algorithm = fifo;
int bakFileDes;
static std::vector< unsigned long > references;

static page_table_row** tlb;
static page_table_row* pageTable;
static frame_table_row* frameTable;
static page_t* memory;
static page_t* swapFile;

static int tlbHits = 0;
static int tlbMisses = 0;
static int numFaults = 0;

#endif /* __MEM_SIM_HPP_ */

