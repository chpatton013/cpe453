#!/bin/sh

time=$1

for ndx in `seq 1 $time`
do
   indent=''
   for i in `seq 1 $ndx`
   do
      indent="$indent   "
   done

   echo "$time$indent$ndx"
   for _ in `seq 0 10000`
   do
      echo >/dev/null
   done
done

