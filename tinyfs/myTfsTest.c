#include "libTinyFS.h"
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define FILE_NOT_EXISTS "fileNotExists"
#define FILE_EXISTS "fileExists"
#define FILE_VALID "fileValid"
#define FILE_INVALID "fileInvalid"

#define REQUIRE_EQ(a, b) \
   do { \
      int eA = a, eB = b; \
      if (!((eA) == (eB))) { \
         fprintf(stderr, "assertion failed: (%d == %d) %s:%d\n", (eA), (eB), \
          __FILE__, __LINE__); \
         teardown(); \
         exit(EXIT_FAILURE); \
      } \
   } while(0)

#define REQUIRE_NEQ(a, b) \
   do { \
      int eA = a, eB = b; \
      if (!((eA) != (eB))) { \
         fprintf(stderr, "assertion failed: (%d != %d) %s:%d\n", (eA), (eB), \
          __FILE__, __LINE__); \
         teardown(); \
         exit(EXIT_FAILURE); \
      } \
   } while(0)

#define REQUIRE_GT(a, b) \
   do { \
      int eA = a, eB = b; \
      if (!((eA) > (eB))) { \
         fprintf(stderr, "assertion failed: (%d > %d) %s:%d\n", (eA), (eB), \
          __FILE__, __LINE__); \
         teardown(); \
         exit(EXIT_FAILURE); \
      } \
   } while(0)

#define REQUIRE_LT(a, b) \
   do { \
      int eA = a, eB = b; \
      if (!((eA) < (eB))) { \
         fprintf(stderr, "assertion failed: (%d < %d) %s:%d\n", (eA), (eB), \
          __FILE__, __LINE__); \
         teardown(); \
         exit(EXIT_FAILURE); \
      } \
   } while(0)

#define MAGIC 0x45
#define PACKED_STRUCT struct __attribute__((__packed__))
typedef PACKED_STRUCT block_head {
   unsigned char type;
   unsigned char magic;
   unsigned char addr;
   unsigned char empty;
} block_head_t;

#define BLOCK_DATA_SIZE (BLOCKSIZE - sizeof(block_head_t))
typedef PACKED_STRUCT block {
   block_head_t head;
   unsigned char data[BLOCK_DATA_SIZE];
} block_t;

static void setup();
static void teardown();
static void wrapTest(void (*test)());

static void testFormat();
static void testMount();
static void testUnmount();
static void testMountAndUnmount();
static void testFileCreationAndDeletion();
static void testBulkWriteAndRead();
static void testSeek();
static void testIncrementalWriteAndRead();
static void testDirCreationAndDeletion();
static void testNestedDirCreationAndDeletion();

static const char* getErrorMessage(int error);

int main(int argc, char** argv) {
   setup();

   testFormat();
   testMount();
   testUnmount();
   testMountAndUnmount();

   wrapTest(testFileCreationAndDeletion);
   wrapTest(testBulkWriteAndRead);
   wrapTest(testSeek);
   wrapTest(testIncrementalWriteAndRead);
   wrapTest(testDirCreationAndDeletion);
   wrapTest(testNestedDirCreationAndDeletion);

   teardown();

   return 0;
}

void setup() {
   printf("initializing disk files\n");

   // FILE_NOT_EXISTS
   unlink(FILE_NOT_EXISTS);
   REQUIRE_NEQ(access(FILE_NOT_EXISTS, F_OK), 0);

   // FILE_EXISTS
   unlink(FILE_EXISTS);
   int existsFd = open(FILE_EXISTS, O_RDWR | O_CREAT, (mode_t)0664);
   REQUIRE_NEQ(existsFd, -1);
   REQUIRE_NEQ(lseek(existsFd, DEFAULT_DISK_SIZE, SEEK_SET), -1);
   REQUIRE_NEQ(close(existsFd), -1);

   // FILE_VALID
   unlink(FILE_VALID);
   int validFd = open(FILE_VALID, O_RDWR | O_CREAT, (mode_t)0664);
   REQUIRE_NEQ(validFd, -1);
   int ndx;
   for (ndx = 0; ndx < DEFAULT_DISK_SIZE / BLOCKSIZE; ++ndx) {
      block_t validBlock = {{0}};
      validBlock.head.magic = MAGIC;
      REQUIRE_NEQ(write(validFd, &validBlock, BLOCKSIZE), -1);
   }
   REQUIRE_NEQ(close(validFd), -1);

   // FILE_INVALID
   unlink(FILE_INVALID);
   int invalidFd = open(FILE_INVALID, O_RDWR | O_CREAT, (mode_t)0664);
   REQUIRE_NEQ(invalidFd, -1);
   char invalidBuffer[DEFAULT_DISK_SIZE] = {0};
   REQUIRE_NEQ(write(invalidFd, invalidBuffer, DEFAULT_DISK_SIZE), -1);
   REQUIRE_NEQ(close(invalidFd), -1);

   // DEFAULT_DISK
   unlink(DEFAULT_DISK_NAME);
   int defaultFd = open(DEFAULT_DISK_NAME, O_RDWR | O_CREAT, (mode_t)0664);
   REQUIRE_NEQ(defaultFd, -1);
   char defaultBuffer[DEFAULT_DISK_SIZE] = {0};
   REQUIRE_NEQ(write(defaultFd, defaultBuffer, DEFAULT_DISK_SIZE), -1);
   REQUIRE_NEQ(close(defaultFd), -1);
}

void teardown() {
   printf("removing disk files\n");

   unlink(FILE_NOT_EXISTS);
   unlink(FILE_EXISTS);
   unlink(FILE_VALID);
   unlink(FILE_INVALID);
   unlink(DEFAULT_DISK_NAME);
}

void wrapTest(void (*test)()) {
   REQUIRE_EQ(tfs_mkfs(DEFAULT_DISK_NAME, DEFAULT_DISK_SIZE), TFS_SUCCESS);
   REQUIRE_EQ(tfs_mount(DEFAULT_DISK_NAME), TFS_SUCCESS);

   test();

   REQUIRE_EQ(tfs_unmount(), TFS_SUCCESS);
}

void testFormat() {
   printf("testing tfs_mkfs\n");

   printf("non-existant file with size 0\n");
   REQUIRE_EQ(tfs_mkfs(FILE_NOT_EXISTS, 0), TFS_ESIZE);

   printf("non-existant file with size 1\n");
   REQUIRE_EQ(tfs_mkfs(FILE_NOT_EXISTS, 1), TFS_ESIZE);

   printf("non-existant file with size %d\n", DEFAULT_DISK_SIZE);
   REQUIRE_EQ(tfs_mkfs(FILE_NOT_EXISTS, DEFAULT_DISK_SIZE), TFS_SUCCESS);
   REQUIRE_EQ(access(FILE_NOT_EXISTS, R_OK | W_OK), 0);
   unlink(FILE_NOT_EXISTS);

   printf("existant file with size %d\n", DEFAULT_DISK_SIZE);
   REQUIRE_EQ(tfs_mkfs(FILE_EXISTS, DEFAULT_DISK_SIZE), TFS_SUCCESS);

   printf("existant file with size 1\n");
   REQUIRE_EQ(tfs_mkfs(FILE_EXISTS, 1), TFS_ESIZE);

   printf("existant file with size 0\n");
   REQUIRE_EQ(tfs_mkfs(FILE_EXISTS, 0), TFS_SUCCESS);
}

void testMount() {
   printf("testing tfs_mount\n");

   printf("non-existant file\n");
   REQUIRE_EQ(tfs_mount(FILE_NOT_EXISTS), TFS_EACCESS);

   printf("valid file\n");
   REQUIRE_EQ(tfs_mount(FILE_VALID), TFS_SUCCESS);
   tfs_unmount();

   printf("valid file, then non-existant file\n");
   REQUIRE_EQ(tfs_mount(FILE_VALID), TFS_SUCCESS);
   REQUIRE_EQ(tfs_mount(FILE_NOT_EXISTS), TFS_EONEFS);
   tfs_unmount();

   printf("valid file, then valid file\n");
   REQUIRE_EQ(tfs_mount(FILE_VALID), TFS_SUCCESS);
   REQUIRE_EQ(tfs_mount(FILE_VALID), TFS_EONEFS);
   tfs_unmount();

   printf("invalid file\n");
   REQUIRE_EQ(tfs_mount(FILE_INVALID), TFS_ENOTFS);

   printf("invalid file, then valid file\n");
   REQUIRE_EQ(tfs_mount(FILE_INVALID), TFS_ENOTFS);
   REQUIRE_EQ(tfs_mount(FILE_VALID), TFS_SUCCESS);
   tfs_unmount();
}

void testUnmount() {
   printf("testing tfs_unmount\n");

   printf("no file\n");
   REQUIRE_EQ(tfs_unmount(), TFS_ENOFS);

   printf("non-existant file\n");
   REQUIRE_EQ(tfs_mount(FILE_NOT_EXISTS), TFS_EACCESS);
   REQUIRE_EQ(tfs_unmount(), TFS_ENOFS);

   printf("invalid file\n");
   REQUIRE_EQ(tfs_mount(FILE_INVALID), TFS_ENOTFS);
   REQUIRE_EQ(tfs_unmount(), TFS_ENOFS);

   printf("valid file\n");
   REQUIRE_EQ(tfs_mount(FILE_VALID), TFS_SUCCESS);
   REQUIRE_EQ(tfs_unmount(), TFS_SUCCESS);
}

void testMountAndUnmount() {
   printf("testing tfs_mount and tfs_unmount\n");

   printf("valid file, unmount, non-existant file, unmount\n");
   REQUIRE_EQ(tfs_mount(FILE_VALID), TFS_SUCCESS);
   REQUIRE_EQ(tfs_unmount(), TFS_SUCCESS);
   REQUIRE_EQ(tfs_mount(FILE_NOT_EXISTS), TFS_EACCESS);
   REQUIRE_EQ(tfs_unmount(), TFS_ENOFS);

   printf("valid file, unmount, invalid file, unmount\n");
   REQUIRE_EQ(tfs_mount(FILE_VALID), TFS_SUCCESS);
   REQUIRE_EQ(tfs_unmount(), TFS_SUCCESS);
   REQUIRE_EQ(tfs_mount(FILE_INVALID), TFS_ENOTFS);
   REQUIRE_EQ(tfs_unmount(), TFS_ENOFS);

   printf("valid file, unmount, valid file, unmount\n");
   REQUIRE_EQ(tfs_mount(FILE_VALID), TFS_SUCCESS);
   REQUIRE_EQ(tfs_unmount(), TFS_SUCCESS);
   REQUIRE_EQ(tfs_mount(FILE_VALID), TFS_SUCCESS);
   REQUIRE_EQ(tfs_unmount(), TFS_SUCCESS);
}

void testFileCreationAndDeletion() {
   printf("testing file creation and deletion\n");

   printf("creating files 'a', 'b', and 'c'\n");
   fileDescriptor a, b, c;
   a = tfs_openFile("a");
   b = tfs_openFile("b");
   c = tfs_openFile("c");

   REQUIRE_GT(a, -1);
   REQUIRE_GT(b, -1);
   REQUIRE_GT(c, -1);

   printf("listing disk contents\n");
   REQUIRE_GT(tfs_listDisk(), -1);

   printf("deleting files 'a', 'b', and 'c'\n");
   REQUIRE_GT(tfs_deleteFile(a), -1);
   REQUIRE_GT(tfs_deleteFile(b), -1);
   REQUIRE_GT(tfs_deleteFile(c), -1);

   printf("listing disk contents\n");
   REQUIRE_GT(tfs_listDisk(), -1);
}

void initializeFile(fileDescriptor FD, int size) {
   char* buffer = (char*)malloc(size * sizeof(char));

   int ndx;
   for (ndx = 0; ndx < size; ++ndx) {
      buffer[ndx] = '0' + (ndx % 7);
   }
   REQUIRE_EQ(tfs_writeFile(FD, buffer, size), TFS_SUCCESS);

   free(buffer);
}

void verifyFile(fileDescriptor FD, int size) {
   initializeFile(FD, size);

   int ndx;
   char byte;
   for (ndx = 0; ndx < size; ++ndx) {
      REQUIRE_EQ(tfs_readByte(FD, &byte), TFS_SUCCESS);
      REQUIRE_EQ(byte, '0' + (ndx % 7));
   }
}

void testBulkWriteAndRead() {
   printf("testing bulk file write and read\n");

   printf("creating file 'file'\n");
   fileDescriptor FD = tfs_openFile("file");
   REQUIRE_GT(FD, -1);

   verifyFile(FD, 1);
   verifyFile(FD, 252);
   verifyFile(FD, 300);
   verifyFile(FD, 600);

   char hugeBuffer[DEFAULT_DISK_SIZE * 252];
   REQUIRE_EQ(tfs_writeFile(FD, hugeBuffer, DEFAULT_DISK_SIZE * 252), TFS_ESPACE);
}

void testSeek() {
   printf("testing seek\n");

   printf("creating file 'file'\n");
   fileDescriptor FD = tfs_openFile("file");
   REQUIRE_GT(FD, -1);

   // Initialize file.
   char buffer[300];
   int ndx;
   for (ndx = 0; ndx < 300; ++ndx) {
      buffer[ndx] = '0' + (ndx % 7);
   }
   REQUIRE_EQ(tfs_writeFile(FD, buffer, 300), TFS_SUCCESS);

   char byte;
   // Validate entire file.
   REQUIRE_EQ(tfs_seek(FD, 0), TFS_SUCCESS);
   for (ndx = 0; ndx < 300; ++ndx) {
      REQUIRE_EQ(tfs_readByte(FD, &byte), TFS_SUCCESS);
      REQUIRE_EQ(byte, '0' + (ndx % 7));
   }

   // Return to middle 300 bytes and validate.
   REQUIRE_EQ(tfs_seek(FD, 100), TFS_SUCCESS);
   for (ndx = 100; ndx < 200; ++ndx) {
      REQUIRE_EQ(tfs_readByte(FD, &byte), TFS_SUCCESS);
      REQUIRE_EQ(byte, '0' + (ndx % 7));
   }
   // Return to first 300 bytes and validate.
   REQUIRE_EQ(tfs_seek(FD, 0), TFS_SUCCESS);
   for (ndx = 0; ndx < 100; ++ndx) {
      REQUIRE_EQ(tfs_readByte(FD, &byte), TFS_SUCCESS);
      REQUIRE_EQ(byte, '0' + (ndx % 7));
   }
   // Return to last 300 bytes and validate.
   REQUIRE_EQ(tfs_seek(FD, 200), TFS_SUCCESS);
   for (ndx = 200; ndx < 300; ++ndx) {
      REQUIRE_EQ(tfs_readByte(FD, &byte), TFS_SUCCESS);
      REQUIRE_EQ(byte, '0' + (ndx % 7));
   }

   // Validate EOF.
   REQUIRE_EQ(tfs_readByte(FD, &byte), TFS_EDATA);

   // Create hole.
   REQUIRE_EQ(tfs_seek(FD, 600), TFS_SUCCESS);
   // Re-validate first 900 bytes.
   REQUIRE_EQ(tfs_seek(FD, 0), TFS_SUCCESS);
   for (ndx = 0; ndx < 300; ++ndx) {
      REQUIRE_EQ(tfs_readByte(FD, &byte), TFS_SUCCESS);
      REQUIRE_EQ(byte, '0' + (ndx % 7));
   }
   // Validate hole.
   for (ndx = 300; ndx < 600; ++ndx) {
      REQUIRE_EQ(tfs_readByte(FD, &byte), TFS_SUCCESS);
   }

   // Validate EOF.
   REQUIRE_EQ(tfs_readByte(FD, &byte), TFS_EDATA);
}

void testIncrementalWriteAndRead() {
   printf("testing incremental file write and read\n");

   printf("creating file 'file'\n");
   fileDescriptor FD = tfs_openFile("file");
   REQUIRE_GT(FD, -1);

   int ndx;

   // Initialize file.
   for (ndx = 0; ndx < 300; ++ndx) {
      REQUIRE_EQ(tfs_writeByte(FD, '0' + (ndx % 7)), TFS_SUCCESS);
   }

   // Jump to file start.
   REQUIRE_EQ(tfs_seek(FD, 0), TFS_SUCCESS);
   // Validate file content.
   char byte;
   for (ndx = 0; ndx < 300; ++ndx) {
      REQUIRE_EQ(tfs_readByte(FD, &byte), TFS_SUCCESS);
      REQUIRE_EQ(byte, '0' + (ndx % 7));
   }
}

void testDirCreationAndDeletion() {
   printf("testing directory creation and deletion\n");

   printf("creating directories 'a', 'b', and 'c'\n");
   REQUIRE_EQ(tfs_createDir("a"), TFS_SUCCESS);
   REQUIRE_EQ(tfs_createDir("b"), TFS_SUCCESS);
   REQUIRE_EQ(tfs_createDir("c"), TFS_SUCCESS);

   printf("listing disk contents\n");
   REQUIRE_GT(tfs_listDisk(), -1);

   printf("deleting directories 'a', 'b', and 'c'\n");
   REQUIRE_EQ(tfs_removeDir("a"), TFS_SUCCESS);
   REQUIRE_EQ(tfs_removeDir("b"), TFS_SUCCESS);
   REQUIRE_EQ(tfs_removeDir("c"), TFS_SUCCESS);

   printf("listing disk contents\n");
   REQUIRE_GT(tfs_listDisk(), -1);
}

void testNestedDirCreationAndDeletion() {
   printf("testing directory creation and deletion\n");

   printf("creating directories 'a', 'b', and 'c'\n");
   REQUIRE_EQ(tfs_createDir("a"), TFS_SUCCESS);
   REQUIRE_EQ(tfs_createDir("b"), TFS_SUCCESS);
   REQUIRE_EQ(tfs_createDir("c"), TFS_SUCCESS);

   printf("creating nested directories 'a/a', 'a/b', and 'a/c'\n");
   REQUIRE_EQ(tfs_createDir("a/a"), TFS_SUCCESS);
   REQUIRE_EQ(tfs_createDir("a/b"), TFS_SUCCESS);
   REQUIRE_EQ(tfs_createDir("a/c"), TFS_SUCCESS);

   printf("creating nested files 'a/a/a', 'a/a/b', and 'a/a/c'\n");
   REQUIRE_GT(tfs_openFile("a/a/a"), -1);
   REQUIRE_GT(tfs_openFile("a/a/b"), -1);
   REQUIRE_GT(tfs_openFile("a/a/c"), -1);

   printf("listing disk contents\n");
   REQUIRE_GT(tfs_listDisk(), -1);

   printf("recursively deleting directory 'a/a'\n");
   REQUIRE_GT(tfs_removeAll("a/a"), -1);

   printf("listing disk contents\n");
   REQUIRE_GT(tfs_listDisk(), -1);

   printf("recursively deleting directory '/'\n");
   REQUIRE_GT(tfs_removeAll(""), -1);

   printf("listing disk contents\n");
   REQUIRE_GT(tfs_listDisk(), -1);
}

const char* getErrorMessage(int error) {
   if (error > 0) {
      return "No error";
   }

   switch (error) {
   case TFS_SUCCESS:    return "Success";
   case TFS_EACCESS:    return "Unix FS Access Error";
   case TFS_ESIZE:      return "Incompatible TinyFS size";
   case TFS_EONEFS:     return "Only one TinyFS instance may be mounted at a time";
   case TFS_ENOFS:      return "No TinyFS instance is currently mounted";
   case TFS_ENOTFS:     return "File is not a TinyFS instance";
   case TFS_ESPACE:     return "Insufficient space";
   case TFS_ENAME:      return "Invalid name";
   case TFS_ETYPE:      return "Incorrect block type";
   case TFS_EEXISTS:    return "File or directory with that name already exists";
   case TFS_ENOTFOUND:  return "File or directory not found";
   case TFS_ENOTEMPTY:  return "Directory not empty";
   case TFS_EBADFD:     return "Invalid file descriptor";
   case TFS_EDATA:      return "No more data";
   case TFS_ERO:        return "File is read-only";
   case TFS_ERW:        return "File is read-write";
   case TFS_EUNKNOWN:   return "Unknown error";
   default:             return "Error not found";
   }
}

