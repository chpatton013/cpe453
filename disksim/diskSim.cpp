#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits>
#include <vector>
#include <utility>

static const int numCyl = 5000;
static const int maxCyl = numCyl - 1;

int fcfs(const std::vector< int >& access, int initial);
int sstf(const std::vector< int >& access, int initial);
int scan(const std::vector< int >& access, int initial);
int c_scan(const std::vector< int >& access, int initial);
int look(const std::vector< int >& access, int initial);
int c_look(const std::vector< int >& access, int initial);

std::pair< int, int > getMinAndMax(const std::vector< int > v);
int highestUnder(const std::vector< int > v, int upperBound);

int main(int argc, char** argv) {
   if (argc < 2 || argc > 3) {
      fprintf(stderr, "usage: diskSim INITIAL_POSITION [ACCESS_SEQUENCE_FILE]\n");
      exit(1);
   }

   char* end;
   long initial = strtol(argv[1], &end, 10);
   if (initial == 0 && end == argv[1] || initial < -maxCyl || initial > maxCyl) {
      fprintf(stderr,
       "INITIAL_POSITION must be an integer between %d and %d, inclusive\n",
       -maxCyl, maxCyl);
      exit(2);
   }

   std::vector< int > access;
   if (argc == 3) {
      FILE* seqFile = fopen(argv[2], "r");
      if (seqFile == NULL) {
         fprintf(stderr, "Failed to open file %s\n", argv[2]);
         exit(3);
      }

      int cylinder;
      while (fscanf(seqFile, "%d", &cylinder) == 1) {
         if (cylinder < 0 || cylinder > maxCyl) {
            fprintf(stderr,
             "cylinder addresses must be between 0 and %d, inclusive\n",
             maxCyl);
            exit(4);
         }
         access.push_back(cylinder);
      }
      fclose(seqFile);
   } else {
      srand(time(NULL));
      for (int ndx = 0; ndx < 100; ++ndx) {
         access.push_back(rand() % numCyl);
      }
   }

   printf("FCFS %d\n", fcfs(access, initial));
   printf("SSTF %d\n", sstf(access, initial));
   printf("SCAN %d\n", scan(access, initial));
   printf("C-SCAN %d\n", c_scan(access, initial));
   printf("LOOK %d\n", look(access, initial));
   printf("C-LOOK %d\n", c_look(access, initial));

   return 0;
}

int fcfs(const std::vector< int >& access, int initial) {
   int position = initial;
   int travel = 0;

   std::vector< int >::const_iterator itr;
   for (itr = access.begin(); itr != access.end(); ++itr) {
      int next = *itr;
      travel += fabs(position - next);
      position = next;
   }

   return travel;
}

int sstf(const std::vector< int >& access, int initial) {
   int position = initial;
   int travel = 0;
   std::vector< int > a = access;

   while (a.size()) {
      int minNdx;
      int minDiff = std::numeric_limits< int >::max();
      for (int ndx = 0; ndx < a.size(); ++ndx) {
         int diff  = fabs(position - a[ndx]);
         if (diff < minDiff) {
            minDiff = diff;
            minNdx = ndx;
         }
      }

      travel += minDiff;
      position = a[minNdx];

      a.erase(a.begin() + minNdx);
   }

   return travel;
}

int scan(const std::vector< int >& access, int initial) {
   std::pair< int, int > bounds = getMinAndMax(access);
   int lowest = bounds.first;
   int highest = bounds.second;

   bool increasing = initial > 0;
   int position = fabs(initial);

   if (increasing) {
      if (position < lowest) { // below both
         return highest - position;
      } else { // between or above both
         return (maxCyl - position) + (maxCyl - lowest);
      }
   } else {
      if (position > highest) { // above both
         return position - lowest;
      } else { // between or below both
         return position + highest;
      }
   }
}

int c_scan(const std::vector< int >& access, int initial) {
   std::pair< int, int > bounds = getMinAndMax(access);
   int lowest = bounds.first;
   int highest = bounds.second;

   int position = fabs(initial);
   int destination = highestUnder(access, position);

   if (initial < 0) {
      return position + highest;
   } else if (position < lowest) { // below both
      return highest - position;
   } else if (position < highest) { // between
      return (maxCyl - position) + maxCyl + destination;
   } else { // above both
      return (maxCyl - position) + maxCyl + highest;
   }
}

int look(const std::vector< int >& access, int initial) {
   std::pair< int, int > bounds = getMinAndMax(access);
   int lowest = bounds.first;
   int highest = bounds.second;

   bool increasing = initial > 0;
   int position = fabs(initial);

   if (increasing) {
      if (position < lowest) { // below both
         return highest - position;
      } else if (position < highest) { // between
         return (highest - position) + (highest - lowest);
      } else { // above both
         return (position - lowest);
      }
   } else {
      if (position > highest) { // above both
         return position - lowest;
      } else if (position > lowest) { // between
         return (position - lowest) + (highest - lowest);
      } else { // below both
         return (highest - position);
      }
   }
}

int c_look(const std::vector< int >& access, int initial) {
   std::pair< int, int > bounds = getMinAndMax(access);
   int lowest = bounds.first;
   int highest = bounds.second;

   bool increasing = initial > 0;
   int position = fabs(initial);
   int destination = highestUnder(access, position);

   if (increasing) {
      if (position < lowest) { // below both
         return highest - position;
      } else if (position < highest) { // between
         return (highest - position) + (highest - lowest) + (destination - lowest);
      } else { // above both
         return (position - lowest) + highest;
      }
   } else if (position > lowest) { // decreasing, between
      return (position - lowest) + highest - lowest;
   } else { // decreasing, below both
      return highest - position;
   }
}

std::pair< int, int > getMinAndMax(const std::vector< int > v) {
   int highest = std::numeric_limits< int >::min();
   int lowest = std::numeric_limits< int >::max();

   std::vector< int >::const_iterator itr;
   for (itr = v.begin(); itr != v.end(); ++itr) {
      int next = *itr;
      if (next > highest) {
         highest = next;
      }
      if (next < lowest) {
         lowest = next;
      }
   }

   return std::pair< int, int >(lowest, highest);
}

int highestUnder(const std::vector< int > v, int upperBound) {
   int highest = std::numeric_limits< int >::min();

   std::vector< int >::const_iterator itr;
   for (itr = v.begin(); itr != v.end(); ++itr) {
      int next = *itr;
      if (next < upperBound && next > highest) {
         highest = next;
      }
   }

   return highest;
}

