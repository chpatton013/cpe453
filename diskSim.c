#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int values[2000];
int save[2000];

int cmp (const void *a, const void *b) {
    return *(int*)a - *(int*)b;
}
int fcfs(int amt) {
    int i, val, sum = 0;
    
    val = values[0];
    if(val < 0)
        val = val * -1;
    sum += val;
    for (i = 1; i < amt; i++) {
        val = values[i] - values[i-1];
        if(val < 0)
            val = val * -1;
        sum += val;
    }
    return sum;
}

int sstf(int amt) {
    int vals[amt];
    int i;
    for (i = 0; i < amt; i++) {
        vals[i] = values[i];
    }
    
    int j, save, start = values[0], count = 0, min;
    for (i = 0; i < amt; i++) {
        min = abs(vals[i]-start);
        save = i;
        for(j = i; j < amt; j++) {
            if(min > abs(start - vals[j])) {
                save = j;
                min = abs(start-vals[j]);
            }
        }
        count += abs(start-vals[save]);
        start = vals[save];
        vals[save] = vals[i];
        vals[i] = save;
    }
    return count;
}

int scan(int amt) {
    int vals[amt], val, i, j, sum, temp, head = values[0], loc, max;
    for (i = 0; i < amt; i++) {
        vals[i] = values[i];
    }
    qsort(vals, amt, sizeof(int),cmp);
    for (i = 0; i < amt; i++) {
        printf("%d ",vals[i]);
    }
    max = vals[amt];
    for(i = 0;i<amt;i++) {
        if(head == vals[i]) {
            loc = i;
            i = amt;
        }
    }
    for(i=loc;i>=0;i--) {
        printf("%d ->",vals[i]);
        printf("[%d]\n", abs(vals[i]-vals[i-1]));
        sum+=abs(vals[i]-vals[i-1]);
    }
    return sum+max+1;
    printf("%d",loc);
    printf("0-->");
    
    for(i=loc+1;i<amt;i++) {
        printf("%d ->",vals[i]);
        sum+=abs(vals[i]-vals[i+1]);
        printf("[%d]\n", abs(vals[i]-vals[i-1]));
    }

    return sum+max;
}

int look(int amt) {
    int vals[amt], sum = 0, start, temp, i,j;
    for (i = 0; i < amt; i++) {
        vals[i] = values[i];
    }
    start = values[0];
    
    for(i = 0; i < amt; i++) {
        for(j = 0; j < amt - i - 1; j++) {
            if(vals[j] > vals[j+1]) {
                temp = vals[j];
                vals[j] = vals[j+1];
                vals[j+1] = temp;
            }
        }
    }
    for(i = 0, temp = 0; i < amt; i++)
        if(vals[i] < start)
            temp++;
    
    for(i = temp; i < amt; i++)
    {
        sum += abs(start - vals[i]);
        start = vals[i];
    }
    for(i = 0; i < temp; i++)
    {
        sum += abs(start-vals[i]);
        start = vals[i];
    }
    return sum;
}

int clook(int amt) {
    int vals[amt], sum = 0, start, temp, i,j;
    for (i = 0; i < amt; i++) {
        vals[i] = values[i];
    }
    start = values[0];
    
    for(i = 0; i < amt; i++) {
        for(j = 0; j < amt - i - 1; j++) {
            if(vals[j] > vals[j+1]) {
                temp = vals[j];
                vals[j] = vals[j+1];
                vals[j+1] = temp;
            }
        }
    }
    for(i = 0, temp = 0; i < amt; i++)
        if(vals[i] < start)
            temp++;

    for(i = temp; i < amt; i++)
    {
        sum += abs(start - vals[i]);
        start = vals[i];
    }
    for(i = 0; i < temp; i++)
    {
        sum += abs(start-vals[i]);
        start = vals[i];
    }
    return sum;
}

int main (int argc, char * argv[]) {

    FILE *file = fopen(argv[2], "r");
    int x = 0, i = 1, total = 0;
    values[0] = 2000;
    fscanf(file, "%d", &x);
    while(!feof(file)) {
        //printf("%d\n", x);
        values[i++] = x;
        fscanf(file, "%d", &x);
    }

    printf("%d", total);
    printf("\n");
    //int j = 0;
    //for(;j < i; j++)
    //    printf("%d\n", values[j]);
    printf("#for the initial position of +2000\n");
    printf("FCFS %d\n", fcfs(i));
    printf("SSTF %d\n", sstf(i));
    printf("SCAN %d\n", scan(i));
    printf("C-SCAN \n");
    printf("LOOK \n");
    printf("C-LOOK %d\n", clook(i));
    
}
