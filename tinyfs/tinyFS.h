#ifndef __TINY_FS_H__
#define __TINY_FS_H__

/* The default size of the disk and file system block */
#define BLOCKSIZE 256

#define DEFAULT_DISK_SIZE 10240
#define DEFAULT_DISK_NAME "tinyFSDisk"

#endif /* __TINY_FS_H__ */

