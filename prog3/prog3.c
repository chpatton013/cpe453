/**
 * Christopher Patton (chpatton)
 * Samantha Bhuiyan (srbhuiya)
 */

#include <stdio.h>


int main(int argc, char *argv[]) {
    
    FILE *file = fopen(argv[1], "r");
    int x = 0;
    int offset = 0, pageNumber = 0;
    fscanf(file, "%d", &x);
    while(!feof(file))
    {
        printf("%d", x);
        //offset
        printf(" %d ", offset = x & (1 << 8) - 1);
        //page number
        printf(" %d \n", pageNumber = x >> 8 & 255);
        printf("%d ", x);
        printf("%d\n", x / 255 - pageNumber);
        
        
        fscanf(file, "%d", &x);
    }
    
    fclose(file);
    
    
    // FILE *f = fopen(BACKING_STORE.bin, "r");
    return 0;
}
