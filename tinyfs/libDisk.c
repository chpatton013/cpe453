#include "tinyFS.h"
#include "libDisk.h"

#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

///////////////////////////////////////////////////////////////////////////////
// Types, Prototypes, and Data.
///////////////////////////////////////////////////////////////////////////////

#define WRITE_BUFFER_SIZE 1024
#define DISK_ARRAY_SIZE 16

static int addDisk(int fd);
static int getFD(int disk);

static int* disk_array = NULL;
static int disk_array_size = 0;
static int disk_array_length = 0;

///////////////////////////////////////////////////////////////////////////////
// Public API.
///////////////////////////////////////////////////////////////////////////////

int openDisk(char* filename, int nBytes) {
   if (nBytes % BLOCKSIZE) {
      return -3;
   }

   if (nBytes == 0 && access(filename, R_OK | W_OK) == -1) {
      return -2;
   }

   int flags = O_RDWR | O_CREAT;
   mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH;
   int fd = open(filename, flags, mode);
   if (fd == -1) {
      return -1;
   }

   // Protected AND prevents re-formatting previously existant file.
   // Set filesize.
   if ((nBytes != 0) && (ftruncate(fd, nBytes) == -1)) {
      close(fd);
      return -1;
   }

   return addDisk(fd);
}

int closeDisk(int disk) {
   int fd = getFD(disk);
   return close(fd);
}

int readBlock(int disk, int bNum, void* block) {
   int fd = getFD(disk);

   if (lseek(fd, bNum * BLOCKSIZE, SEEK_SET) == -1) {
      return -1;
   }

   return read(fd, block, BLOCKSIZE) == -1 ? -1 : 0;
}

int writeBlock(int disk, int bNum, void* block) {
   int fd = getFD(disk);

   if (lseek(fd, bNum * BLOCKSIZE, SEEK_SET) == -1) {
      return -1;
   }

   return write(fd, block, BLOCKSIZE) == -1 ? -1 : 0;
}

///////////////////////////////////////////////////////////////////////////////
// Private API.
///////////////////////////////////////////////////////////////////////////////

int addDisk(int fd) {
   if (disk_array == NULL) {
      disk_array_length = DISK_ARRAY_SIZE;
      int size = disk_array_length * sizeof(int);
      disk_array = (int*)malloc(size);
   }

   if (disk_array_size >= disk_array_length) {
      disk_array_length *= 2;
      int size = disk_array_length * sizeof(int);
      disk_array = (int*)realloc(disk_array, size);
   }

   int disk = disk_array_size;
   disk_array[disk] = fd;

   ++disk_array_size;
   return disk;
}

int getFD(int disk) {
   if (disk < 0 || disk >= disk_array_size) {
      return -1;
   }

   return disk_array[disk];
}

