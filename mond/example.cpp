#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define COUNT 20
#define TIME 500

int main(void) {
   void* mem[COUNT];

   for (int ndx = 0; ndx < COUNT; ++ndx) {
      mem[ndx] = malloc(1024);
      usleep(TIME);
   }

   for (int ndx = 0; ndx < COUNT; ++ndx) {
      free(mem[ndx]);
      usleep(TIME);
   }

   return 0;
}

