#include <stdlib.h>
#include <stdio.h>
#include "lwp.h"

/* Public API. */

/* Function declarations. */
int new_lwp(lwpfun, void*, size_t);
void lwp_exit();
int lwp_getpid();
void lwp_yield();
void lwp_start();
void lwp_stop();
void lwp_set_scheduler(schedfun sched);

/* Extern data. */
lwp_context lwp_ptable[LWP_PROC_LIMIT];
int lwp_procs = 0;
int lwp_running = -1;

/* End Public API. */


/* Private API. */

#define DFLT_STACK_SIZE 1024
#define push(sp,val) (*(--sp) = (ptr_int_t)(val))
#define index_to_context(ndx) ((ndx) < 0 ? &masterContext : lwp_ptable + (ndx))

ptr_int_t* new_intel_stack(ptr_int_t* sp, lwpfun func, void* arg) {
   ptr_int_t* ebp;

   push(sp, arg);
   push(sp, lwp_exit);
   push(sp, func);
   push(sp, 0x00000000);

   ebp = sp;

   push(sp, 0x00000000);
   push(sp, 0x00000000);
   push(sp, 0x00000000);
   push(sp, 0x00000000);
   push(sp, 0x00000000);
   push(sp, 0x00000000);
#ifdef __x86_64
   push(sp, 0x00000000);
   push(sp, 0x00000000);
   push(sp, 0x00000000);
   push(sp, 0x00000000);
   push(sp, 0x00000000);
   push(sp, 0x00000000);
   push(sp, 0x00000000);
   push(sp, 0x00000000);
#endif
   push(sp, ebp);

   return sp;
}

/* Function declarations. */
static void context_switch(int from, int to);
static int rr_sched();

/* Static global data. */
static lwp_context masterContext;
static schedfun scheduler = rr_sched;
static ptr_int_t* deallocationQueue = NULL;

/* End Private API. */


/* Function definitions. */

/* Public API. */

int new_lwp(lwpfun func, void* arg, size_t stacksize) {
   static int pid = 1;

   ptr_int_t* stack;

   if (lwp_procs >= LWP_PROC_LIMIT) {
      return -1;
   }

   if (stacksize == 0) {
      stacksize = DFLT_STACK_SIZE;
   }

   stack = (ptr_int_t*)calloc(stacksize, sizeof(ptr_int_t));
   if (!stack) {
      return -1;
   }

   /* Initialize context. */
   lwp_context* ctx = lwp_ptable + lwp_procs++;
   ctx->pid = pid++;
   ctx->stack = stack;
   ctx->stacksize = stacksize;
   ctx->sp = new_intel_stack(stack + stacksize, func, arg);

   return ctx->pid;
}

void lwp_exit() {
   int ndx;
   lwp_context* ctx;
   ptr_int_t* deallocate;

   if (lwp_running < 0) {
      return;
   }

   /* Add this lwp's stack to the deallocation queue. */
   deallocate = index_to_context(lwp_running)->sp;
   deallocate[0] = (ptr_int_t)deallocationQueue;
   deallocationQueue = deallocate;

   /* Shift all other lwp's down. */
   --lwp_procs;
   for (ndx = lwp_running; ndx < lwp_procs; ++ndx) {
      lwp_ptable[ndx] = lwp_ptable[ndx + 1];
   }

   /* Edge cases. Force switch back to master or first lwp. */
   if (!lwp_procs) {
      lwp_running = -1;
   } else if (lwp_procs == lwp_running) {
      lwp_running = 0;
   }

   /* Switch to next lwp */
   ctx = index_to_context(lwp_running);
   SetSP(ctx->sp);
   RESTORE_STATE();
}

int lwp_getpid() {
   return lwp_ptable[lwp_running].pid;
}

void lwp_yield() {
   if (lwp_running < 0) {
      return;
   }

   context_switch(lwp_running, scheduler());
}

void lwp_start() {
   if (lwp_running >= 0) {
      return;
   }

   context_switch(-1, scheduler());
}

void lwp_stop() {
   ptr_int_t* itr;

   if (lwp_running < 0) {
      return;
   }

   for (itr = deallocationQueue; itr; itr = (ptr_int_t*)itr[0]) {
      free(itr);
   }
   deallocationQueue = NULL;

   context_switch(lwp_running, -1);
}

void lwp_set_scheduler(schedfun sched) {
   scheduler = sched;
}

/* Private API. */

void context_switch(int from, int to) {
   lwp_context* from_ctx = index_to_context(from);
   lwp_context* to_ctx = index_to_context(to);

   SAVE_STATE();
   GetSP(from_ctx->sp);

   lwp_running = to;
   SetSP(to_ctx->sp);
   RESTORE_STATE();
}

static int rr_sched() {
   static int next = 0;

   next %= lwp_procs;
   return next++;
}

/* End Function definitions. */

