#ifndef __MOND_HPP_
#define __MOND_HPP_

#include <stdio.h>
#include <unistd.h>

struct proc_stat {
   long unsigned int utime;
   long unsigned int stime;
   long unsigned int idletime;
   long unsigned int iowait;
   long unsigned int irq;
   long unsigned int softirq;
   long unsigned int intr;
   long unsigned int ctxt;
   unsigned int processes;
   unsigned int procs_running;
   unsigned int procs_blocked;
};

struct proc_meminfo {
   long unsigned int total;
   long unsigned int free;
   long unsigned int cached;
   long unsigned int swapcached;
   long unsigned int active;
   long unsigned int inactive;
};

struct proc_loadavg {
   float one;
   float five;
   float fifteen;
};

struct proc_diskstats {
   long unsigned int numReads;
   long unsigned int readSectors;
   long unsigned int readTime;
   long unsigned int numWrites;
   long unsigned int writeSectors;
   long unsigned int writeTime;
};

struct proc_pid_stat {
   pid_t pid;
   char comm[256];
   char state;
   pid_t ppid;
   pid_t pgrp;
   int session;
   int tty_nr;
   int tpgid;
   unsigned int flags;
   long unsigned int minflt;
   long unsigned int cminflt;
   long unsigned int majflt;
   long unsigned int cmajflt;
   long unsigned int utime;
   long unsigned int stime;
   long int cutime;
   long int cstime;
   long int priority;
   long int nice;
   long int num_threads;
   long int itrealvalue;
   long long unsigned int starttime;
   long unsigned int vsize;
   long int rss;
   long int rsslim;
};

struct proc_pid_statm {
   int size;
   int rss;
   int share;
   int text;
   int lib;
   int data;
};

struct system_status {
   struct proc_stat stat;
   struct proc_meminfo meminfo;
   struct proc_loadavg loadavg;
   struct proc_diskstats diskstats;
};

struct process_status {
   struct proc_pid_stat stat;
   struct proc_pid_statm statm;
};

void usage();

enum parse_reason {INTERVAL};
void parse(parse_reason reason);

pid_t spawnChild(char* executable);

proc_stat getSysStat();
proc_meminfo getSysMeminfo();
proc_loadavg getSysLoadavg();
proc_diskstats getSysDiskstats();
system_status getSystemStatus();

proc_pid_stat getProcStat(pid_t pid);
proc_pid_statm getProcStatm(pid_t pid);
process_status getProcessStatus(pid_t pid);

void writeSystemStatus(FILE* fp, system_status& st);
void writeProcessStatus(FILE* fp, process_status& st);

#endif /* __MOND_HPP_ */

