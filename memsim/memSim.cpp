#include "memSim.hpp"

#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <limits>

int main(int argc, char** argv) {
   srand(time(NULL));
   parseArgs(argc, argv);
   initSwap();
   readReferences(refFile);

   tlb = (page_table_row**)calloc(TLB_SIZE, sizeof(page_table_row*));
   pageTable = (page_table_row*)calloc(numPages, sizeof(page_table_row));
   for (int ndx = 0; ndx < numPages; ++ndx) {
      pageTable[ndx].mapping.pageNum = ndx;
   }
   frameTable = (frame_table_row*)calloc(numFrames, sizeof(frame_table_row));
   memory = (page_t*)calloc(numFrames, sizeof(page_t));
   for (int ndx = 0; ndx < numFrames * sizeof(page_t) / sizeof(int); ++ndx) {
      ((int*)memory)[ndx] = rand();
   }

   for (std::vector< unsigned long >::iterator itr = references.begin();
    itr != references.end(); ++itr) {
      unsigned long ref = *itr;
      page_table_row* pageRow = access(ref);
      printAccess(ref, pageRow);
   }

   printf("Number of Translated Addresses = %d\n", references.size());
   printf("Page Faults = %d\n", numFaults);
   printf("Page Fault Rate = %0.3f\n", numFaults / (float)references.size());
   printf("TLB Hits = %d\n", tlbHits);
   printf("TLB Hit Rate = %0.3f\n", tlbHits / (float)(tlbHits + tlbMisses));

   free(memory);
   free(frameTable);
   free(pageTable);
   free(tlb);

   teardownSwap();

   return 0;
}

void parseArgs(int argc, char** argv) {
   if (argc < 2 || argc > 4) {
      usage();
   }

   refFile = fopen(argv[1], "r");
   if (!refFile) {
      parse(REF_FILE);
   }

   if (argc > 2) {
      char* end;
      numFrames = strtol(argv[2], &end, 10);
      if (numFrames == 0 && end == argv[2] || numFrames <= 0 || numFrames > 256) {
         parse(FRAMES);
      }
   }

   if (argc > 3) {
      if (!strcmp(argv[3], "fifo")) {
         algorithm = fifo;
      } else if (!strcmp(argv[3], "lru")) {
         algorithm = lru;
      } else {
         parse(PRA);
      }
   }
}

void usage() {
   fprintf(stderr, "usage: memSim <reference-file.txt> <FRAMES> <PRA>\n");
   exit(2);
}

void parse(parse_error error) {
   switch (error) {
   case REF_FILE:
      fprintf(stderr, "Could not open reference file\n");
      break;
   case FRAMES:
      fprintf(stderr, "FRAMES must be in the interval (0, 256]\n");
      break;
   case PRA:
      fprintf(stderr, "PRA must be 'fifo' or 'lru'\n");
      break;
   default:
      fprintf(stderr, "Unknown parse error\n");
      break;
   }

   exit(3);
}

void initSwap() {
   bakFileDes = open("BACKING_STORE.bin", O_RDWR);
   if (bakFileDes == -1) {
      fprintf(stderr, "Failed to open BACKING_STORE.bin\n");
      exit(1);
   }

   struct stat fileStat;
   if (fstat(bakFileDes, &fileStat) == -1) {
      fprintf(stderr, "Failed to stat BACKING_STORE.bin\n");
      exit(1);
   }

   if (ftruncate(bakFileDes, 65536) == -1) {
      fprintf(stderr, "Failed to truncate BACKING_STORE.bin\n");
      exit(1);
   }

   swapFile = (page_t*)mmap(NULL, 65536, PROT_READ | PROT_WRITE, MAP_SHARED,
    bakFileDes, 0);
   if (swapFile == MAP_FAILED) {
      fprintf(stderr, "Failed to map BACKING_STORE.bin\n");
      exit(1);
   }
}

void teardownSwap() {
   munmap(swapFile, 65536);
   close(bakFileDes);
}

void readReferences(FILE* refFile) {
   unsigned long ref;
   while (fscanf(refFile, "%lu", &ref) == 1) {
      references.push_back(ref);
   }
   fclose(refFile);
}

page_table_row* findTLBRow(int pageNum) {
   for (int ndx = 0; ndx < TLB_SIZE && tlb[ndx]; ++ndx) {
      if (pageNum == tlb[ndx]->mapping.pageNum) {
         ++tlbHits;
         return tlb[ndx];
      }
   }

   ++tlbMisses;
   return NULL;
}

page_table_row* access(unsigned long ref) {
   int pageNum = ref / PAGE_SIZE;

   page_table_row* tlbRow = findTLBRow(pageNum);
   if (tlbRow && tlbRow->loaded) {
      if (algorithm == lru) {
         lru(tlbRow->mapping.frameNum);
      }

      return tlbRow;
   }

   page_table_row* pageRow = pageTable + pageNum;
   if (!tlbRow) {
      // Shift TLB down 1 page row.
      memcpy(tlb, tlb + 1, sizeof(page_table_row*) * (TLB_SIZE - 1));
      tlb[TLB_SIZE - 1] = pageRow;
   }

   int frameNum = -1;
   if (!pageRow->loaded) {
      ++numFaults;

      // Look for an inactive frame.
      for (int ndx = 0; ndx < numFrames; ++ndx) {
         if (!frameTable[ndx].active) {
            frameNum = ndx;
            break;
         }
      }

      if (frameNum < 0) {
         static unsigned long long ullMax =
          std::numeric_limits< unsigned long long >::max();

         // Find the least-valued frame.
         unsigned long long when = ullMax;
         for (int ndx = 0; ndx < numFrames; ++ndx) {
            if (frameTable[ndx].when < when) {
               frameNum = ndx;
               when = frameTable[ndx].when;
            }
         }
      }

      if (algorithm == fifo) {
         fifo(frameNum);
      }

      swap(pageNum, frameNum);
   }

   if (algorithm == lru) {
      lru(pageRow->mapping.frameNum);
   }

   return pageRow;
}

void fifo(int frameNum) {
   static unsigned long long iteration = 1;
   frameTable[frameNum].when = iteration++;
}

void lru(int frameNum) {
   ++frameTable[frameNum].when;
}

void swap(int pageNum, int frameNum) {
   // Mark the current page for this frame as inactive.
   for (int ndx = 0; ndx < numPages; ++ndx) {
      page_table_row* row = pageTable + ndx;
      if (row->mapping.frameNum == frameNum && row->loaded) {
         // Swap out this frame.
         if (frameTable[frameNum].active) {
            memcpy(swapFile + ndx, memory + frameNum, PAGE_SIZE);
         }

         row->loaded = false;
         break;
      }
   }

   // Swap in desired frame.
   memcpy(memory + frameNum, swapFile + pageNum, PAGE_SIZE);
   frameTable[frameNum].active = true;
   pageTable[pageNum].loaded = true;
   pageTable[pageNum].mapping.frameNum = frameNum;
}

void printAccess(unsigned long ref, page_table_row* row) {
   int frameNum = row->mapping.frameNum;
   unsigned long base = frameNum * PAGE_SIZE;
   unsigned long offset = base + ref % PAGE_SIZE;

   printf("%lu, %d, %d, ", ref, ((char*)memory)[offset], row->mapping.frameNum);
   /* printFrame(frameNum); */
   printf("\n");
}

void printFrame(int frameNum) {
   unsigned long base = frameNum * PAGE_SIZE;
   for (int ndx = 0; ndx < PAGE_SIZE / sizeof(unsigned long); ++ndx) {
      printf("%x", memory[base + ndx]);
   }
}

