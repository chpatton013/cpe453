#ifndef __LIB_TINY_FS_H__
#define __LIB_TINY_FS_H__

#include "tinyFS.h"
#include "TinyFS_errno.h"
#include <stdint.h>
#include <time.h>

typedef int fileDescriptor;

/**
 * Makes a blank TinyFS file system of size 'nBytes' on the file specified by
 * 'filename'. This function should use the emulated disk library to open the
 * specified file, and upon success, format the file to be mountable. This
 * includes initializing all data to 0x00, setting magic numbers, initializing
 * and writing the superblock and inodes, etc. Returns success/error codes.
 */
int tfs_mkfs(char* filename, int nBytes);

/**
 * Mounts a TinyFS file system located within 'filename'. Should verify the file
 * system is the correct type. Only one file system may be mounted as a time.
 * Returns success/error codes.
 */
int tfs_mount(char* filename);

/**
 * Unmounts the currently mounted file system. Returns success/error codes.
 */
int tfs_unmount();

/**
 * Lists all files and directories on the file system. Returns success/error
 * codes.
 */
int tfs_listDisk();

/**
 * Alias for tfs_listDisk().
 */
int tfs_readdir();

/**
 * Creates a directory. Returns success/error codes.
 */
int tfs_createDir(char* dirName);

/**
 * Deletes an empty directory. Returns success/error codes.
 */
int tfs_removeDir(char* dirName);

/**
 * Lists the content a directory. Returns success/error codes.
 */
int tfs_listDir(char* dirName);

/**
 * Recursively deletes directory and any files or directories under it. Returns
 * success/error codes.
 */
int tfs_removeAll(char* dirName);

/**
 * Opens a file for reading (and possibly writing) on the currently mounted file
 * system. Creates a dynamic resource table entry for the file, and returns a
 * file descriptor that can be used to reference this file while the file system
 * is mounted.
 */
fileDescriptor tfs_openFile(char* name);

/**
 * Closes the file, de-allocates all system/disk resources, and removes table
 * entry. Returns success/error codes.
 */
int tfs_closeFile(fileDescriptor FD);

/**
 * Writes buffer 'buffer' of size 'size', which represents an entire file's
 * content, to the file system. Sets the file pointer to 0 (the start of the
 * file) when done. Returns success/error codes.
 */
int tfs_writeFile(fileDescriptor FD, char* buffer, int size);

/**
 * Deletes a file and marks its blocks as free on disk. Returns success/error
 * codes.
 */
int tfs_deleteFile(fileDescriptor FD);

/**
 * Reads one byte from the tile and copies it to the buffer using the current
 * file pointer location and incrementing it by one upon success. If the file
 * pointer is already at the end of the file then tfs_readByte() should return
 * an error and not increment the file pointer.
 */
int tfs_readByte(fileDescriptor FD, char* buffer);

/**
 * Writes one byte to file at current file pointer location and incrementing it
 * by one upon success.
 */
int tfs_writeByte(fileDescriptor FD, unsigned char data);

/**
 * Change the file pointer location to given absolute offset.
 */
int tfs_seek(fileDescriptor FD, int offset);

/**
 * Renames a file or directory. Returns success/error codes.
 */
int tfs_rename(fileDescriptor FD, char* newName);

/**
 * Makes the file read only. If a file is RO, all calls to tfs_removeDir(),
 * tfs_removeAll(), tfs_write(), tfs_deleteFile(), tsf_writeByte(), and
 * tfs_rename() fail. Returns success/error codes.
 */
int tfs_makeRO(char* name);

/**
 * Makes the file read only. Returns success/error codes.
 */
int tfs_makeRW(char* name);

/**
 * Returns true if the file is RO, false otherwise.
 */
int tfs_getRO(fileDescriptor FD);

/**
 * Returns true if the file is RW, false otherwise.
 */
int tfs_getRW(fileDescriptor FD);

/**
 * Returns the file size.
 */
uint64_t tfs_getSize(fileDescriptor FD);

/**
 * Returns the file creation time.
 */
time_t tfs_getCTime(fileDescriptor FD);

/**
 * Returns the file modification time.
 */
time_t tfs_getMTime(fileDescriptor FD);

/**
 * Returns the file accessed time.
 */
time_t tfs_getATime(fileDescriptor FD);

/**
 * Alias for tfs_getCTime().
 */
time_t tfs_readFileInfo(fileDescriptor FD);

#endif /* __LIB_TINY_FS_H__ */

